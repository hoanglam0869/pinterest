import {
  unsaveImageService,
  getCreatedImagesByUserService,
  getSavedImagesByUserService,
  saveImageService,
  updateCommentService,
  deleteCommentService,
} from "./services.js";
import { createLoadingImages } from "./setup/header.js";
import { loginPopup } from "./setup/setup.js";
import {
  qS,
  CrEl,
  hideLoading,
  showToast,
  urlIncludes,
  A,
  Div,
  Img,
  Button,
  DropDown,
  showLoading,
  qSA,
} from "./utils.js";

let createdImages = null;
let savedImages = null;
let isClickable = true;

export const showImages = (images, user_id, savedImgs, tabs = null) => {
  let status = qS(".status");
  status.innerHTML = "";
  if (images.length == 0) {
    status.innerHTML = `<p class="text-center">Chưa có Ghim nào, nhưng còn rất nhiều cơ hội</p>`;
    return;
  }
  createLoadingImages(images.length);
  let galleryItems = qSA(".gallery-item");

  galleryItems.forEach((galleryItem, i) => {
    // lấy hình đã tạo và đã lưu
    let image = images[i];
    if (image.image) image = image.image;
    let { image_id, image_url, image_name } = image;

    let imageCon = A("image-link", galleryItem, true);
    imageCon._href(`./detail.html?image_id=${image_id}`);

    let figure = CrEl("figure", "", imageCon);
    Img("", figure)._src(image_url);
    CrEl("figcaption", "", figure)._text(image_name);

    let overlay = Div("overlay", figure);
    let btnWrapper = Div("ms-auto", overlay);

    let index = savedImgs.findIndex((s) => s.image_id == image_id);
    if (index > -1) {
      createUnsaveBtn(btnWrapper, user_id, image_id, tabs);
    } else {
      createSaveBtn(btnWrapper, user_id, image_id, tabs);
    }
    CrEl("p", "", galleryItem)._text("");
  });
  /* images.forEach((image) => {
    // lấy hình đã tạo và đã lưu
    if (image.image) image = image.image;
    let { image_id, image_url, image_name } = image;

    let imageCon = A("image-link", gallery);
    imageCon._href(`./detail.html?image_id=${image_id}`);

    let galleryItem = Div("gallery-item", imageCon);
    let figure = CrEl("figure", "", galleryItem);

    Img("", figure)._src(image_url);
    CrEl("figcaption", "", figure)._text(image_name);

    let overlay = Div("overlay", figure);
    let btnWrapper = Div("ms-auto", overlay);

    let index = savedImgs.findIndex((s) => s.image_id == image_id);
    if (index > -1) {
      createUnsaveBtn(btnWrapper, user_id, image_id, tabs);
    } else {
      createSaveBtn(btnWrapper, user_id, image_id, tabs);
    }
    CrEl("p", "", galleryItem)._text("");
  }); */
};

function createSaveBtn(btnWrapper, user_id, image_id, tabs) {
  let saveBtn = Button("red-btn pill-btn", btnWrapper, true);
  saveBtn._text("Lưu");

  saveBtn.onclick = async (e) => {
    e.preventDefault();
    // kiểm tra xem người dùng đăng nhập chưa
    if (user_id == 0) {
      loginPopup.show();
      return;
    }
    if (!isClickable) return;
    isClickable = false;
    // Tạo nút đã lưu hình
    createUnsaveBtn(btnWrapper, user_id, image_id, tabs);
    try {
      let res = await saveImageService(user_id, image_id);
      showToast(res.data.message, true);
      // Đang ở trang người dùng không
      if (urlIncludes("user.html")) {
        await reloadImages(user_id, tabs);
      }
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createUnsaveBtn(btnWrapper, user_id, image_id, tabs) {
  let unsaveBtn = Button("black-btn pill-btn", btnWrapper, true);
  unsaveBtn._text("Đã lưu");

  unsaveBtn.onclick = async (e) => {
    e.preventDefault();

    if (!isClickable) return;
    isClickable = false;
    // Tạo nút lưu hình
    createSaveBtn(btnWrapper, user_id, image_id, tabs);
    try {
      let res = await unsaveImageService(user_id, image_id);
      showToast(res.data.message, true);
      // Đang ở trang người dùng không
      if (urlIncludes("user.html")) {
        await reloadImages(user_id, tabs);
      }
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

async function reloadImages(user_id, tabs) {
  let ref_user_id = window.location.href.split("=")[1];
  if (user_id != ref_user_id) return;
  savedImages = [];
  try {
    let res = await getSavedImagesByUserService(user_id);
    let savedImgs = res.data.content;

    getCreatedImages(ref_user_id, user_id, savedImgs, tabs);
    getSavedImages(ref_user_id, user_id, savedImgs, tabs);
  } catch (err) {
    hideLoading(err);
  }
}

export function getCreatedImages(ref_user_id, user_id, savedImgs, tabs) {
  try {
    tabs.setClickOnTab(0, async (tabContent) => {
      Div("status", tabContent, true);
      let gallery = Div("gallery", tabContent);

      if (createdImages == null) {
        showLoading(gallery, { backgroundColor: "transparent" });
        let res = await getCreatedImagesByUserService(ref_user_id);
        createdImages = res.data.content;
        hideLoading();
      }
      showImages(createdImages, user_id, savedImgs, tabs);
    });
  } catch (err) {
    hideLoading(err);
  }
}

export function getSavedImages(ref_user_id, user_id, savedImgs, tabs) {
  try {
    tabs.setClickOnTab(1, async (tabContent) => {
      Div("status", tabContent, true);
      let gallery = Div("gallery", tabContent);

      if (savedImages == null) {
        showLoading(gallery, { backgroundColor: "transparent" });
        let res = await getSavedImagesByUserService(ref_user_id);
        savedImages = res.data.content;
        hideLoading();
      }
      showImages(savedImages, user_id, savedImgs, tabs);
    });
  } catch (err) {
    hideLoading(err);
  }
}

export const addComment = (comment, user_id) => {
  let cmItem = document.createElement("div");
  cmItem.className = "comment-item";

  createComment(comment, cmItem, user_id);
  return cmItem;
};

function createComment(comment, cmItem, user_id) {
  let { user, content, comment_date } = comment;
  let { avatar, full_name } = user;
  let ref_user_id = user.user_id;

  let cmLeft = Div("comment-left me-2", cmItem, true);
  let userImg = A("", cmLeft)._href(`./user.html?user_id=${ref_user_id}`);
  let avatarCon = Div("avatar sm-avatar", userImg);

  if (avatar == "") {
    Div("no-avatar", avatarCon)._text(full_name.charAt(0).toUpperCase());
  } else {
    Img("comment-avatar w-100", avatarCon)._src(avatar);
  }
  let cmRight = Div("comment-right", cmItem);
  let cmContent = Div("comment-content", cmRight);

  let userName = A("name-link me-2", cmContent)._text(full_name);
  userName._href(`./user.html?user_id=${ref_user_id}`);

  CrEl("span", "", cmContent)._text(content);

  let cmInteraction = Div("comment-interaction", cmRight);
  CrEl("span", "me-3", cmInteraction)._text(formatCommentDate(comment_date));

  let reply = CrEl("b", "c-pointer mx-3", cmInteraction)._text("Trả lời");
  reply.onclick = () => {
    if (user_id == 0) {
      loginPopup.show();
    } else {
      showReplyInput(cmItem);
    }
  };
  createCommentReactions(cmInteraction, user_id);

  let info = { user_id, ref_user_id, comment };
  createMoreInteractions(cmInteraction, cmItem, info);
}

function formatCommentDate(comment_date) {
  let before = Date.parse(comment_date);
  let distance = Date.now() - before;

  if (distance < 60 * 1000) {
    return "Mới đây";
  } else if (distance < 60 * 60 * 1000) {
    return Math.floor(distance / (60 * 1000)) + " phút";
  } else if (distance < 24 * 60 * 60 * 1000) {
    return Math.floor(distance / (60 * 60 * 1000)) + " giờ";
  } else if (distance < 7 * 24 * 60 * 60 * 1000) {
    return Math.floor(distance / (24 * 60 * 60 * 1000)) + " ngày";
  } else if (distance < 52 * 7 * 24 * 60 * 60 * 1000) {
    return Math.floor(distance / (7 * 24 * 60 * 60 * 1000)) + " tuần";
  } else {
    return Math.floor(distance / (52 * 7 * 24 * 60 * 60 * 1000)) + " năm";
  }
}

function showReplyInput(cmItem) {
  qS(".reply-wrapper")?.remove();

  let replyWrapper = document.createElement("div");
  replyWrapper.className = "reply-wrapper mt-2 ms-5";
  cmItem.insertAdjacentElement("afterend", replyWrapper);

  let replyTop = Div("position-relative", replyWrapper);
  let commentControl = Div("comment-control", replyTop);

  let emojiBtn = Div("emoji-btn", commentControl);
  let emojiIcon = Div("emoji-icon", emojiBtn)._text("😃");

  let replyInput = CrEl("input", "reply-input", replyTop);
  replyInput._attr({ placeholder: "Trả lời", type: "text" });

  let dropdown = DropDown({
    element: emojiIcon,
    action: "click",
    position: "drop-up-start",
    distX: 15,
    distY: -6,
    callback: (dropdownMenu) => {
      dropdownMenu.classList.add("p-0");
      let emojiPicker = CrEl("emoji-picker", "", dropdownMenu);

      emojiPicker.addEventListener("emoji-click", (e) => {
        dropdown.hide();
        replyInput.focus();
        replyInput.value += e.detail.emoji.unicode;
        replyInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    },
  });
  let replyBottom = Div("text-end mt-2", replyWrapper);
  let cancelBtn = Button("gray-btn pill-btn me-2", replyBottom)._text("Hủy");
  cancelBtn.onclick = () => replyWrapper.remove();
  let saveBtn = Button("disabled-btn pill-btn", replyBottom)._text("Lưu");

  replyInput.oninput = (e) => {
    if (e.target.value.trim() == "") {
      saveBtn.className = "disabled-btn pill-btn";
    } else {
      saveBtn.className = "red-btn pill-btn";
    }
  };
}

function createCommentReactions(wrapper, user_id) {
  let reaction = Div("comment-reaction mx-3", wrapper);
  let reactionIcon = Div("comment-reaction-icon", reaction);
  reactionIcon.style.backgroundImage = "url(./img/reaction.svg)";

  let container = Div("comment-reaction-container", reaction);

  let item1 = Div("comment-reaction-item", container);
  let move1 = Div("comment-reaction-move", item1);
  move1.style.backgroundImage = "url(./img/comment-loved.svg)";
  Div("comment-reaction-text", move1)._text("Yêu thích");
  item1.onclick = () => {
    if (user_id == 0) {
      loginPopup.show();
    } else {
      //
    }
  };
  let item2 = Div("comment-reaction-item", container);
  let move2 = Div("comment-reaction-move", item2);
  move2.style.backgroundImage = "url(./img/comment-helpful.svg)";
  Div("comment-reaction-text", move2)._text("Hữu ích");
  item2.onclick = () => {
    if (user_id == 0) {
      loginPopup.show();
    } else {
      //
    }
  };
}

function createMoreInteractions(cmInteraction, cmItem, info) {
  let { user_id, ref_user_id, comment } = info;

  let drdControl = Div("mx-3", cmInteraction);
  let icon = Div("icon small-icon", drdControl);
  CrEl("i", "fa fa-ellipsis-h", icon);

  let dropdown = DropDown({
    element: drdControl,
    action: "click",
    position: "drop-down-center",
    distX: 0,
    distY: 10,
    callback: (dropdownMenu) => {
      if (user_id == ref_user_id) {
        let updateCm = Div("drop-down-item", dropdownMenu)._text("Chỉnh sửa");
        updateCm.onclick = () => {
          dropdown.hide();
          showUpdateInput(cmItem, user_id, comment);
        };
        let deleteCm = Div("drop-down-item", dropdownMenu)._text("Xóa");
        deleteCm.onclick = async () => {
          try {
            dropdown.hide();
            showLoading(cmItem);
            await deleteCommentService(comment.comment_id);
            cmItem.remove();
          } catch (err) {
            hideLoading();
          }
        };
      } else {
        Div("drop-down-item", dropdownMenu)._text("Báo cáo nội dung này");
        Div("drop-down-item", dropdownMenu)._text("Chặn người dùng");
      }
    },
  });
}

function showUpdateInput(cmItem, user_id, comment) {
  let updateWrapper = Div("flex-grow-1 mt-2", cmItem, true);

  let updateTop = Div("position-relative", updateWrapper);
  let commentControl = Div("comment-control", updateTop);

  let emojiBtn = Div("emoji-btn", commentControl);
  let emojiIcon = Div("emoji-icon", emojiBtn)._text("😃");

  let updateInput = CrEl("input", "reply-input", updateTop);
  updateInput._attr({ placeholder: "Trả lời", type: "text" });
  updateInput._attr({ value: comment.content });

  let dropdown = DropDown({
    element: emojiIcon,
    action: "click",
    position: "drop-up-start",
    distX: 15,
    distY: -6,
    callback: (dropdownMenu) => {
      dropdownMenu.classList.add("p-0");
      let emojiPicker = CrEl("emoji-picker", "", dropdownMenu);

      emojiPicker.addEventListener("emoji-click", (e) => {
        dropdown.hide();
        updateInput.focus();
        updateInput.value += e.detail.emoji.unicode;
        updateInput.dispatchEvent(new Event("input", { bubbles: true }));
      });
    },
  });
  let updateBottom = Div("text-end mt-2", updateWrapper);
  let cancelBtn = Button("gray-btn pill-btn me-2", updateBottom)._text("Hủy");
  cancelBtn.onclick = () => createComment(comment, cmItem, user_id);

  let saveBtn = Button("disabled-btn pill-btn", updateBottom)._text("Lưu");
  updateInput.oninput = (e) => {
    if (e.target.value == comment.content) {
      saveBtn.className = "disabled-btn pill-btn";
      saveBtn.onclick = () => {};
    } else {
      saveBtn.className = "red-btn pill-btn";
      saveBtn.onclick = async () => {
        let content = updateInput.value;
        try {
          showLoading(cmItem, { right: "-4px", bottom: "-4px" });
          let res = await updateCommentService(comment.comment_id, content);
          let updateComment = res.data.content;

          createComment(updateComment, cmItem, user_id);
          hideLoading();
        } catch (err) {
          hideLoading();
        }
      };
    }
  };
}
