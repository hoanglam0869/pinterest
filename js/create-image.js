import { loginPopup, setup } from "./setup/setup.js";
import { uploadImageService } from "./services.js";
import {
  qS,
  CrEl,
  hideLoading,
  showLoading,
  showToast,
  Popup,
  Div,
} from "./utils.js";
import { showUrlPopup } from "./popup/urlPopup.js";

let dropZone = qS("#drop-zone");
let imageInput = qS(".image-input");
let uploadCon = qS("#upload-wrapper");
let imageArr = [];
let popup = Popup();

setup((user, savedImages) => {
  let { user_id } = user;
  showImage(user_id);
  handleUrlPopup(user_id);
  hideLoading();
});

function showImage(user_id) {
  imageInput.onchange = () => {
    const clickFile = imageInput.files[0];
    if (!clickFile) return;
    imageArr.push(clickFile);
    const fr = new FileReader();
    fr.readAsDataURL(clickFile);
    fr.onloadend = () => handleImage(fr.result, user_id);
  };
}

function handleUrlPopup(user_id) {
  qS("#show-url-popup").onclick = () => {
    popup = Popup("url-popup", (popupContent) => {
      showUrlPopup(popupContent, user_id, popup, (myFile, result) => {
        imageArr.push(myFile);
        handleImage(result, user_id);
      });
    });
    popup.show();
  };
}

function handleImage(imageURL, user_id) {
  let img = new Image();
  img.src = imageURL;
  img.onload = () => {
    dropZone.style.height = "auto";
    let chosenImage = Div("chosen-image", dropZone, true);
    let width = chosenImage.offsetWidth;
    // style cho hình được chọn
    chosenImage.style.height = (width * img.height) / img.width + "px";
    chosenImage.style.backgroundImage = `url(${img.src})`;
    chosenImage.style.backgroundRepeat = "no-repeat";
    chosenImage.style.backgroundSize = "cover";
    // hiện nút đăng hình
    let uploadBtn = CrEl("button", "red-btn pill-btn", uploadCon);
    uploadBtn._text("Đăng");
    uploadBtn.onclick = () => createImage(user_id);
  };
}

function toDataURL(url) {
  let arr = url.split("/");
  let fileName = arr[arr.length - 1].replace(/(\.jpg)|(\.png)|(\.jpeg)/g, "");
  fetch(url)
    .then((res) => res.blob())
    .then((blob) => {
      const fr = new FileReader();
      fr.readAsDataURL(blob);
      fr.onloadend = () => {
        handleImage(fr.result);
        dataURLtoFile(fr.result, fileName);
      };
    });
}

function dataURLtoFile(dataurl, filename) {
  let arr = dataurl.split(",");
  let mime = arr[0].match(/:(.*?);/)[1];
  let bstr = atob(arr[1]);
  let n = bstr.length;
  let u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  let file = new File([u8arr], filename, { type: mime });
  imageArr.push(file);
}

async function createImage(user_id) {
  if (user_id == 0) {
    loginPopup.show();
    return;
  }
  let image_name = qS("#image-title").value;
  let image_desc = qS("#image-desc").value;

  if (image_name.trim() == "") {
    showToast("Tên hình không được để trống", false);
    return;
  }
  try {
    showLoading();
    let res = await uploadImageService(
      imageArr[0],
      image_name,
      image_desc,
      user_id
    );
    hideLoading();

    let { image_id } = res.data.content;
    window.location.href = `./detail.html?image_id=${image_id}`;
  } catch (err) {
    hideLoading(err);
  }
}

/* dropZone.addEventListener('click', () => inputElement.click());
dropZone.addEventListener('dragover', (e) => {
  e.preventDefault();
});
dropZone.addEventListener('drop', (e) => {
  e.preventDefault();
  img.style = "display:block;";
  let file = e.dataTransfer.files[0];

  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = function () {
    e.preventDefault()
    p.style = 'display: none';
    let src = this.result;
    img.src = src;
    img.alt = file.name
  }
}); */
