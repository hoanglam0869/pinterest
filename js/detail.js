import { addComment } from "./controller.js";
import { loginPopup, setup, signupPopup } from "./setup/setup.js";
import {
  createImageReactionService,
  deleteImageReactionService,
  unsaveImageService,
  getCommentsService,
  getImageDetailService,
  getImageReactionByUserService,
  hasSavedImageService,
  saveCommentService,
  saveImageService,
  updateImageReactionService,
  hasFollowedUserService,
  followUserService,
  unfollowUserService,
  getThreeMostImageReactionsService,
  getImageReactionsAmountService,
  getFollowersAmountService,
} from "./services.js";
import {
  qS,
  qSA,
  Popup,
  RotateArrow,
  CrEl,
  hideLoading,
  showLoading,
  showToast,
  Div,
  Button,
  Img,
  A,
  DropDown,
} from "./utils.js";
import { showUpdateImagePopup } from "./popup/updateImagePopup.js";
import { clearReactions, showReactionPopup } from "./popup/reactionPopup.js";

let card = qS(".card");
let cardImage = qS(".card-image");
let cardDetail = qS(".card-detail");
let cardTop = qS(".card-top");
let cardBottom = qS(".card-bottom");
let handleIcon = qS("#handle-icon");
let saveCon = qS("#save-wrapper");
let label = qS("#label");
let followCon = qS("#follow-wrapper");
let commentList = qS("#comment-list");
let wrapper = qS("#comment-wrapper");
let count = qS("#count");
let reaction = qS("#reaction-circle");
let reactionResult = qS(".reaction-result");
let reactionAmount = qS(".reaction-amount");
let reactionBtn = qS("#reaction-btn");
let reactionCon = qS("#reaction-container");
let reactionStatus = qSA(".reaction-status");
let addCommentWrapper = qS("#add-comment-wrapper");
let popup = Popup();
let dropdown = DropDown();
let updateImgItem;

const icons = [
  {
    name: "good idea",
    img: "./img/good_idea.svg",
    bgColor: "#fffebb",
  },
  {
    name: "love",
    img: "./img/love.svg",
    bgColor: "#ffe0e0",
  },
  {
    name: "thanks",
    img: "./img/thanks.svg",
    bgColor: "#ccf6ee",
  },
  {
    name: "wow",
    img: "./img/wow.svg",
    bgColor: "#fff0db",
  },
  {
    name: "haha",
    img: "./img/haha.svg",
    bgColor: "#e9e4ff",
  },
];

let image_id = window.location.href.split("=")[1];
let isClickable = true;

setup(async (user, savedImages) => {
  showLoading(card);
  handleIcon.classList.remove("d-none");
  let { user_id } = user;
  if (user_id == 0) {
    handleNotLoggedIn();
    await handleImageDetail(user_id);
    await getThreeMostIcons();
    await loadComments(user_id);
  } else {
    await handleImageDetail(user_id);
    await handleSaveButton(user_id);
    await handleReaction(user_id);
    await loadComments(user_id);
    handleComment(user);
  }
  hideLoading();
});

async function handleNotLoggedIn() {
  // thêm nút lưu hình
  let saveBtn = Button("red-btn pill-btn", saveCon, true)._text("Lưu");
  saveBtn.onclick = () => loginPopup.show();
  // xử lý nút theo dõi
  let followBtn = Button("gray-btn pill-btn", followCon, true);
  followBtn._text("Theo dõi");
  followBtn.onclick = () => loginPopup.show();
  // xử lý nút thích
  qS("#reaction").onclick = () => loginPopup.show();
  reaction.style.backgroundColor = "#e9e9e9";
  reactionBtn.style.backgroundImage = "url(./img/reaction.svg)";
  // xử lý nhận xét
  let loginBtn = CrEl("b", "c-pointer", addCommentWrapper)._text("Đăng nhập");
  CrEl("span", "mx-1", addCommentWrapper)._text("hoặc");
  let signupBtn = CrEl("b", "c-pointer", addCommentWrapper)._text("Đăng ký");
  CrEl("span", "ms-1", addCommentWrapper)._text("để thêm nhận xét.");
  loginBtn.onclick = () => loginPopup.show();
  signupBtn.onclick = () => signupPopup.show();
  // xử lý số lượng lượt thích
  handleReactionPopup(true);
}

async function handleImageDetail(user_id) {
  try {
    let res = await getImageDetailService(image_id);

    let { image_url, image_name, image_desc } = res.data.content;
    let ref_user = res.data.content.user;

    let ref_user_id = ref_user.user_id;
    let { avatar, full_name } = ref_user;
    // xử lý hình ảnh
    handleImageSize(image_url);
    // hình ảnh chi tiết
    Img("w-100", cardImage, true)._src(image_url);
    hideLoading();
    showLoading(cardDetail);
    // xử lý các nút
    handleIconButtons(image_url, image_name);
    // thông tin hình
    let canUpdate = user_id == ref_user_id;
    handleImageInfo(image_url, image_name, image_desc, canUpdate);
    // thông tin người tạo ảnh
    await handleImageCreator(user_id, ref_user_id, avatar, full_name);
  } catch (err) {
    hideLoading(err);
  }
}

function handleImageSize(image_url) {
  let docWidth = window.innerWidth;
  let height = window.innerHeight - 120;

  let img = new Image();
  img.src = image_url;
  img.onload = () => {
    let offsetHeight = (cardImage.offsetWidth * img.height) / img.width;
    if (docWidth < 992) {
      cardImage.style.height = offsetHeight + "px";
      let fixHeight = offsetHeight > 600 ? offsetHeight : 600;
      cardDetail.style.height = fixHeight + "px";
    } else {
      let fixHeight = offsetHeight > 600 ? offsetHeight : 600;
      fixHeight = offsetHeight > height ? offsetHeight : height;
      cardImage.style.height = fixHeight + "px";
      cardDetail.style.height = fixHeight + "px";
    }
  };
  // sự kiện xoay màn hình
  screen.orientation.addEventListener("change", function () {
    handleImageSize(image_url);
  });
  // sự kiện đổi kích thước cửa sổ
  window.onresize = () => {
    handleImageSize(image_url);
    setOverflow();
  };
}

export function handleImageInfo(image_url, image_name, image_desc, canUpdate) {
  let imageInfo = qS("#image-info");
  // liên kết hình
  let imageLink = A("", imageInfo, true)._attr({ target: "_blank" });
  imageLink._href(image_url);
  // tên hình
  CrEl("h4", "", imageLink)._text(image_name);
  // mô tả
  let descCon = Div("mt-3", imageInfo);

  let fixImageDesc = image_desc.replace(/\n/g, "<br>");
  descCon._html(fixImageDesc);

  if (image_desc.length > 50) {
    createShowMoreBtn(fixImageDesc, descCon);
  }
  // drop down
  if (!canUpdate) return;
  if (!updateImgItem) {
    dropdown.setItem(true, (dropdownItem) => {
      updateImgItem = dropdownItem;
      updateImgItem.innerText = "Chỉnh sửa Ghim";
    });
  }
  updateImgItem.onclick = () => {
    let image = { image_id, image_name, image_desc };
    popup = Popup("update-image-popup", (popupContent) => {
      showUpdateImagePopup(image, popupContent, popup, (imgName, imgDesc) => {
        handleImageInfo(image_url, imgName, imgDesc, canUpdate);
      });
    });
    popup.show();
  };
}

function createShowMoreBtn(image_desc, descCon) {
  CrEl("span", "", descCon, true)._html(image_desc.substring(0, 230));
  let showMore = CrEl("b", "c-pointer", descCon)._text(" ... thêm");

  showMore.onclick = () => {
    createShowLessBtn(image_desc, descCon);
    setOverflow();
  };
}

function createShowLessBtn(image_desc, descCon) {
  CrEl("span", "", descCon, true)._html(image_desc);
  let showLess = CrEl("b", "c-pointer", descCon)._text(" ... bớt");

  showLess.onclick = () => {
    createShowMoreBtn(image_desc, descCon);
    setOverflow();
  };
}

async function handleImageCreator(user_id, ref_user_id, avatar, full_name) {
  // liên kết người dùng
  let userLink = qS("#user-link");
  userLink.href = `./user.html?user_id=${ref_user_id}`;
  // ảnh đại diện
  let userAvatar = Div("avatar md-avatar me-2", userLink, true);
  if (avatar == "") {
    Div("no-avatar", userAvatar)._text(full_name.charAt(0).toUpperCase());
  } else {
    Img("w-100", userAvatar)._src(avatar);
  }
  let div = Div("", userLink);
  // họ tên
  CrEl("b", "", div)._text(full_name);
  // người theo dõi
  try {
    let res = await getFollowersAmountService(ref_user_id);
    let total = res.data.content;

    Div("total-follower", div)._text(total + " người theo dõi");

    // xóa nút theo dõi khi là người dùng chính
    if (user_id == 0) return;
    if (user_id == ref_user_id) {
      followCon.innerHTML = "";
      return;
    }
    res = await hasFollowedUserService(user_id, ref_user_id);
    let hasFollowedUser = res.data.content;

    if (hasFollowedUser) {
      createUnfollowBtn(user_id, ref_user_id, total);
    } else {
      createFollowBtn(user_id, ref_user_id, total);
    }
  } catch (err) {
    hideLoading(err);
  }
}

function handleIconButtons(image_url, image_name) {
  let drd1 = Div("drd1", handleIcon, true);
  let div1 = Div("icon normal-icon", drd1);
  CrEl("i", "fa fa-ellipsis-h", div1);
  dropdown = DropDown({
    element: drd1,
    action: "click",
    position: "drop-down-center",
    distX: 0,
    distY: 10,
    callback: (dropdownMenu) => {
      let dlImg = Div("drop-down-item", dropdownMenu);
      dlImg._text("Tải hình ảnh xuống");
      dlImg.onclick = () => downloadImage(image_url, image_name);

      Div("drop-down-item", dropdownMenu)._text("Ẩn Ghim");
    },
  });
  const url = window.location.href;
  let drd2 = Div("drd2", handleIcon);
  let div2 = Div("icon normal-icon", drd2);
  CrEl("i", "fa fa-share", div2);
  DropDown({
    element: drd2,
    action: "click",
    position: "drop-down-center",
    distX: 0,
    distY: 10,
    callback: (dropdownMenu) => {
      Div("text-center", dropdownMenu)._text("Chia sẻ");
      let div = Div("d-flex mt-4", dropdownMenu);
      // Whatsapp
      let a1 = A("social-icon", div);
      Img("", a1)._src("./img/whatsapp.png");
      CrEl("span", "", a1)._text("Whatsapp");
      a1.onclick = () => shareWhatsApp(url);
      // Messenger
      let a2 = A("social-icon", div);
      Img("", a2)._src("./img/messenger.png");
      CrEl("span", "", a2)._text("Messenger");
      a2.onclick = () => shareMessenger(url);
      // Facebook
      let a3 = A("social-icon", div);
      Img("", a3)._src("./img/facebook.png");
      CrEl("span", "", a3)._text("Facebook");
      a3.onclick = () => shareFacebook(url);
      // Twitter
      let a4 = A("social-icon", div);
      Img("", a4)._src("./img/twitter.png");
      CrEl("span", "", a4)._text("X");
      a4.onclick = () => shareTwitter(url);
    },
  });
  let copyLink = Div("icon normal-icon", handleIcon);
  CrEl("i", "fa fa-link", copyLink);
  copyLink.onclick = () => copyToClipboard(url);
}

function downloadImage(image_url, image_name) {
  fetch(image_url)
    .then((res) => res.blob())
    .then((file) => {
      let tempURL = URL.createObjectURL(file);
      let aTag = document.createElement("a");
      aTag.href = tempURL;
      aTag.download = image_name;
      document.body.appendChild(aTag);
      aTag.click();
      aTag.remove();
      URL.revokeObjectURL(tempURL);
      showToast("Tải hình ảnh thành công", true);
    })
    .catch(() => {
      showToast("Tải hình ảnh thất bại", false);
    });
}

function shareWhatsApp(url) {
  let link = `https://api.whatsapp.com/send?text=${encodeURIComponent(url)}`;
  window.open(link, "_blank");
}

function shareMessenger(url) {
  const isMobile = () => {
    const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /iPad/i,
      /iPod/i,
      /BlackBerry/i,
      /Windows Phone/i,
    ];
    return toMatch.some((toMatchItem) => {
      return navigator.userAgent.match(toMatchItem);
    });
  };
  if (isMobile()) {
    let popup = `left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0`;
    window.open("fb-messenger://share/?link=" + url, "", popup);
  } else {
    FB.ui({
      method: "send",
      link: url,
    });
  }
}

function shareFacebook(url) {
  let shareURL = "http://facebook.com/sharer/sharer.php?";
  shareURL += `u=${encodeURIComponent(url)}`;
  let popup = `left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0`;
  window.open(shareURL, "", popup);
}

function shareTwitter(url) {
  // let shareURL = "http://twitter.com/share?";
  let shareURL = "http://twitter.com/intent/tweet?";
  let params = {
    url: url,
    text: "Hey, I found something interesting!",
    // via: "@LamCan2806",
    hashtags: "pinterest",
  };
  for (let prop in params) {
    shareURL += `&${`${prop}=${encodeURIComponent(params[prop])}`}`;
  }
  window.open(shareURL, "_blank");
}

function copyToClipboard(url) {
  if (navigator.clipboard) {
    navigator.clipboard.writeText(url);
  } else {
    const textArea = document.createElement("textarea");
    textArea.value = url;
    document.body.appendChild(textArea);
    textArea.focus({ preventScroll: true });
    textArea.select();
    try {
      document.execCommand("copy");
    } catch (err) {
      console.error("Unable to copy to clipboard", err);
    }
    document.body.removeChild(textArea);
  }
  showToast("Đã sao chép liên kết", true);
}

async function handleSaveButton(user_id) {
  try {
    let res = await hasSavedImageService(user_id, image_id);
    let isSavedImage = res.data.content;
    // kiểm tra hình đã lưu hay chưa
    if (isSavedImage) {
      createUnsaveBtn(user_id);
    } else {
      createSaveBtn(user_id);
    }
  } catch (err) {
    hideLoading(err);
  }
}

function createSaveBtn(user_id) {
  let saveBtn = Button("red-btn pill-btn", saveCon, true)._text("Lưu");

  saveBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;
    // hiện nút đã lưu hình
    createUnsaveBtn(user_id);
    try {
      let res = await saveImageService(user_id, image_id);
      showToast(res.data.message, true);
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createUnsaveBtn(user_id) {
  let unsaveBtn = Button("black-btn pill-btn", saveCon, true)._text("Đã lưu");

  unsaveBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;
    // hiện nút lưu hình
    createSaveBtn(user_id);
    try {
      let res = await unsaveImageService(user_id, image_id);
      showToast(res.data.message, true);
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createFollowBtn(user_id, ref_user_id, total) {
  let followBtn = Button("gray-btn pill-btn", followCon, true);
  followBtn._text("Theo dõi");

  followBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;

    qS(".total-follower")._text(++total + " người theo dõi");
    createUnfollowBtn(user_id, ref_user_id, total);
    try {
      await followUserService(user_id, ref_user_id);
      showToast("Theo dõi người dùng thành công", true);
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createUnfollowBtn(user_id, ref_user_id, total) {
  let followingBtn = Button("black-btn pill-btn", followCon, true);
  followingBtn._text("Đang theo dõi");

  followingBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;

    qS(".total-follower")._text(--total + " người theo dõi");
    createFollowBtn(user_id, ref_user_id, total);
    try {
      await unfollowUserService(user_id, ref_user_id);
      showToast("Hủy theo dõi người dùng thành công", true);
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

async function handleReaction(user_id) {
  await getReactionByUser(user_id);
  await getThreeMostIcons();
  handleReactionPopup(true);

  reaction.onclick = (e) => {
    if (reactionCon.contains(e.target)) return;
    if (reaction.classList.contains("active")) {
      deleteImageReaction(user_id);
    } else {
      createImageReaction("love", user_id);
    }
  };
  reactionStatus.forEach((status, i) => {
    status.onclick = () => {
      if (reaction.classList.contains("active")) {
        updateImageReaction(status.getAttribute("type"), user_id);
      } else {
        createImageReaction(status.getAttribute("type"), user_id);
      }
    };
  });
}

async function getReactionByUser(user_id) {
  try {
    let res = await getImageReactionByUserService(user_id, image_id);
    let isReacted = res.data.content;

    if (isReacted == null) {
      reaction.classList.remove("active");

      reaction.style.backgroundColor = "#e9e9e9";
      reactionBtn.style.backgroundImage = `url(./img/reaction.svg)`;
    } else {
      reaction.classList.add("active");
      let index = icons.findIndex((i) => i.name == isReacted.reaction_type);

      reaction.style.backgroundColor = icons[index].bgColor;
      reactionBtn.style.backgroundImage = `url(${icons[index].img})`;
    }
  } catch (err) {
    hideLoading(err);
  }
}

async function getThreeMostIcons() {
  try {
    let res = await getThreeMostImageReactionsService(image_id);
    let threeMostIcons = res.data.content;

    let html = threeMostIcons.map((icon) => {
      let index = icons.findIndex((i) => i.name == icon.reaction_type);
      return `<div class="result-icon" style="background-image: url(${icons[index].img})"></div>`;
    });
    reactionResult.innerHTML = html.join("");

    res = await getImageReactionsAmountService(image_id);
    let amount = res.data.content;
    reactionAmount.innerText = amount > 0 ? amount : "";
  } catch (err) {
    hideLoading(err);
  }
}

async function createImageReaction(reaction_type, user_id) {
  if (!isClickable) return;
  isClickable = false;

  reaction.classList.add("active");
  let index = icons.findIndex((icon) => icon.name == reaction_type);

  reaction.style.backgroundColor = icons[index].bgColor;
  reactionBtn.style.backgroundImage = `url(${icons[index].img})`;
  try {
    await createImageReactionService(reaction_type, user_id, image_id);

    await getThreeMostIcons();
    handleReactionPopup(true);
    isClickable = true;
  } catch (err) {
    hideLoading(err);
  }
}

async function deleteImageReaction(user_id) {
  if (!isClickable) return;
  isClickable = false;

  reaction.classList.remove("active");
  reaction.style.backgroundColor = "#e9e9e9";
  reactionBtn.style.backgroundImage = `url(./img/reaction.svg)`;
  try {
    await deleteImageReactionService(user_id, image_id);

    await getThreeMostIcons();
    handleReactionPopup(true);
    isClickable = true;
  } catch (err) {
    hideLoading(err);
  }
}

async function updateImageReaction(reaction_type, user_id) {
  if (!isClickable) return;
  isClickable = false;

  reaction.classList.add("active");
  let index = icons.findIndex((icon) => icon.name == reaction_type);

  reaction.style.backgroundColor = icons[index].bgColor;
  reactionBtn.style.backgroundImage = `url(${icons[index].img})`;
  try {
    await updateImageReactionService(reaction_type, user_id, image_id);

    await getThreeMostIcons();
    handleReactionPopup(true);
    isClickable = true;
  } catch (err) {
    hideLoading(err);
  }
}

function handleReactionPopup(isRefresh = false) {
  if (isRefresh) clearReactions();
  reactionResult.onclick = () => {
    Popup("reaction-popup", async (popupContent) => {
      let html = [reactionResult.innerHTML, reactionAmount.innerText];
      // hiển thị reaction popup
      showReactionPopup(popupContent, html, image_id, icons);
    }).show();
  };
}

function handleComment(user) {
  let { user_id, full_name, avatar } = user;

  let div = Div("", addCommentWrapper, true);
  let div1 = Div("avatar md-avatar me-2", div);
  if (avatar == "") {
    Div("no-avatar", div1)._text(full_name.charAt(0).toUpperCase());
  } else {
    CrEl("img", "w-100", div1).src = avatar;
  }
  let div2 = Div("flex-grow-1 position-relative", addCommentWrapper);
  let commentControl = Div("comment-control", div2);

  let emojiBtn = Div("emoji-btn", commentControl);
  let emojiIcon = Div("emoji-icon", emojiBtn)._text("😃");
  let saveCm = Div("", commentControl);

  let commentTxt = CrEl("input", "comment-input", div2);
  commentTxt._attr({ placeholder: "Thêm nhận xét", type: "text" });

  let dropdown = DropDown({
    element: emojiIcon,
    action: "click",
    position: "drop-up-start",
    distX: 15,
    distY: -6,
    callback: (dropdownMenu) => {
      dropdownMenu.classList.add("p-0");
      let emojiPicker = CrEl("emoji-picker", "", dropdownMenu);

      emojiPicker.addEventListener("emoji-click", (e) => {
        dropdown.hide();
        commentTxt.focus();
        commentTxt.value += e.detail.emoji.unicode;
        commentTxt.dispatchEvent(new Event("input", { bubbles: true }));
      });
    },
  });
  commentTxt.oninput = (e) => {
    if (e.target.value.trim() == "") {
      saveCm.innerHTML = "";
    } else {
      let saveCmBtn = Button("red-btn search-btn", saveCm, true);
      CrEl("i", "fa fa-paper-plane", saveCmBtn);
      saveCmBtn.onclick = async () => {
        if (!isClickable) return;
        isClickable = false;
        await saveComment(user_id, commentTxt);
        isClickable = true;
      };
    }
  };
}

async function loadComments(user_id) {
  commentList.innerHTML = "";
  try {
    let res = await getCommentsService(image_id);
    let comments = res.data.content;

    if (comments.length > 0) {
      CrEl("span", "", label, true)._text("Nhận xét");
      let showComments = Div("icon normal-icon ms-2", label);
      // ẩn/hiện bình luận
      RotateArrow({
        selector: showComments,
        dir1: "down",
        dir2: "right",
      }).setClick((destDir) => {
        commentList.style.display = destDir == "down" ? "block" : "none";
        setOverflow();
      });
      // danh sách bình luận
      comments.forEach((comment) => {
        commentList.appendChild(addComment(comment, user_id));
      });
      setOverflow();
      count._text(`${comments.length} Nhận xét`);
    } else {
      CrEl("span", "", label, true)._text("Nhận xét");
      commentList.innerHTML = `Chưa có nhận xét nào! Thêm nhận xét để bắt đầu cuộc trò chuyện.`;
      setOverflow();
      count._text("Bạn nghĩ gì?");
    }
  } catch (err) {
    hideLoading(err);
  }
}

async function saveComment(user_id, commentTxt) {
  let content = commentTxt.value;
  if (content.trim() == "") {
    showToast("Không được để trống", false);
    return;
  }
  try {
    showLoadingComments();
    let res = await saveCommentService(content, user_id, image_id);
    showToast("Thêm nhận xét thành công", true);

    commentTxt.value = "";
    commentTxt.dispatchEvent(new Event("input", { bubbles: true }));

    let comment = res.data.content;
    commentList.prepend(addComment(comment, user_id));
    hideLoading();
  } catch (err) {
    hideLoading(err);
  }
}

function setOverflow() {
  wrapper.style.overflow = "hidden";
  if (wrapper.scrollHeight > wrapper.offsetHeight) {
    wrapper.style.overflow = "clip scroll";
  }
}

/* window.onscrollend = () => {
  handleCardTopAndBottom();
}; */

function handleCardTopAndBottom() {
  let rect1 = cardDetail.getBoundingClientRect();
  let left = rect1.left;
  let right = rect1.right;
  console.log(left, right);
  let rect1Top = rect1.top;
  let rect1Bottom = rect1.bottom;

  if (rect1Top < 64) {
    cardTop.classList.add("dock");
    cardTop.style = `top: 64px; left: ${left}px; width: ${
      right - left
    }px; background-color: #fff; border-radius: 0 0 32px 32px`;
  } else {
    cardTop.classList.remove("dock");
  }
  let docHeight = window.innerHeight;
  if (rect1Bottom > docHeight) {
    cardBottom.classList.add("dock");
    cardBottom.style = `bottom: 0; left: ${left}px; width: ${
      right - left
    }px; background-color: #fff; border-radius: 0`;
  } else {
    cardBottom.classList.remove("dock");
  }
}

function showLoadingComments() {
  let rect1 = commentList.getBoundingClientRect();
  let pos1 = 0;
  if (rect1.top < 0) {
    pos1 = Math.abs(rect1.top) + 64;
  } else if (rect1.top < 64) {
    pos1 = 64 - rect1.top;
  }

  let rect2 = cardBottom.getBoundingClientRect();
  let pos2 = rect1.bottom - rect2.top;

  let styles = {
    top: `${pos1 - 24}px`,
    left: "-24px",
    right: "-24px",
    bottom: `${pos2 - 24}px`,
    backgroundColor: "transparent",
  };
  showLoading(commentList, styles);
}
