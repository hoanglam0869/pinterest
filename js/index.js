import { showImages } from "./controller.js";
import { setup } from "./setup/setup.js";
import { getImagesService } from "./services.js";
import { hideLoading, showLoading } from "./utils.js";
import { createLoadingImages, search } from "./setup/header.js";

let image_name = window.location.href.split("=")[1];

createLoadingImages();

setup(async (user, savedImages) => {
  showLoading();
  let { user_id } = user;
  if (image_name) {
    await search(image_name, user_id, savedImages);
  } else {
    await getImages(user_id, savedImages);
  }
  hideLoading();
});

async function getImages(user_id, savedImages) {
  try {
    let res = await getImagesService();
    showImages(res.data.content, user_id, savedImages);
  } catch (err) {
    hideLoading(err);
  }
}
