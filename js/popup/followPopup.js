import {
  followUserService,
  getFollowersService,
  getFollowingsService,
  unfollowUserService,
} from "../services.js";
import { loginPopup } from "../setup/setup.js";
import {
  A,
  CrEl,
  Div,
  Img,
  hideLoading,
  showLoading,
  showToast,
} from "../utils.js";

let followers = null;
let followings = null;
let mainFollows = null;
let isClickable = true;

export async function showFollowersPopup(popupContent, info, popup) {
  let { amount, ref_user_id, user_id } = info;
  let popupHeader = CrEl("h3", "popup-header", popupContent);
  popupHeader._text(amount + " Người theo dõi");

  let popupBody = Div("popup-body", popupContent);
  await getFollowers(ref_user_id, popupBody);
  await getMainFollows(user_id, popupBody);
  showFollows(user_id, followers, popupBody, popup);
}

async function getFollowers(ref_user_id, popupBody) {
  if (followers != null) return;
  try {
    showLoading(popupBody);
    let res = await getFollowersService(ref_user_id);
    followers = res.data.content;
    hideLoading();
  } catch (err) {
    hideLoading(err);
  }
}

export async function showFollowingsPopup(popupContent, info, popup) {
  let { amount, ref_user_id, user_id } = info;
  let popupHeader = CrEl("h3", "popup-header", popupContent);
  popupHeader._text(amount + " Người đang theo dõi");

  let popupBody = Div("popup-body", popupContent);
  await getFollowings(ref_user_id, popupBody);
  await getMainFollows(user_id, popupBody);
  showFollows(user_id, followings, popupBody, popup);
}

async function getFollowings(ref_user_id, popupBody) {
  if (followings != null) return;
  try {
    showLoading(popupBody);
    let res = await getFollowingsService(ref_user_id);
    followings = res.data.content;
    hideLoading();
  } catch (err) {
    hideLoading(err);
  }
}

async function getMainFollows(user_id, popupBody) {
  if (mainFollows != null) return;
  try {
    showLoading(popupBody);
    let res = await getFollowingsService(user_id);
    mainFollows = res.data.content;
    hideLoading();
  } catch (err) {
    hideLoading(err);
  }
}

function showFollows(user_id, follows, popupBody, popup) {
  follows.forEach((follow) => {
    let { avatar, full_name } = follow.user;
    let ref_user_id = follow.user.user_id;

    let popupItem = Div("popup-item", popupBody);

    let aTag = A("d-flex align-items-center", popupItem);
    aTag.href = `./user.html?user_id=${ref_user_id}`;

    let avatarCon = Div("avatar md-avatar me-2", aTag);
    if (avatar == "") {
      Div("no-avatar", avatarCon)._text(full_name.charAt(0).toUpperCase());
    } else {
      Img("w-100", avatarCon)._src(avatar);
    }
    CrEl("span", "", aTag)._text(full_name);

    let btnCon = Div("follow-user", popupItem);
    if (user_id == 0) {
      let followBtn = CrEl("button", "red-btn pill-btn", btnCon);
      followBtn._text("Theo dõi");
      followBtn.onclick = () => {
        popup.hide();
        loginPopup.show();
      };
    } else if (user_id == ref_user_id) {
      CrEl("button", "disabled-btn pill-btn", btnCon)._text("Chính là bạn!");
    } else {
      let index = mainFollows.findIndex((f) => f.user.user_id == ref_user_id);
      if (index == -1) {
        createFollowBtn(user_id, ref_user_id, btnCon);
      } else {
        createUnfollowBtn(user_id, ref_user_id, btnCon);
      }
    }
  });
  popupBody.style.overflow = "hidden";
  if (popupBody.scrollHeight > popupBody.offsetHeight) {
    popupBody.style.overflow = "clip scroll";
  }
}

function createFollowBtn(user_id, ref_user_id, btnWrapper) {
  let followBtn = CrEl("button", "red-btn pill-btn", btnWrapper, true);
  followBtn._text("Theo dõi");

  followBtn.onclick = async (e) => {
    if (!isClickable) return;
    isClickable = false;
    createUnfollowBtn(user_id, ref_user_id, btnWrapper);
    try {
      await followUserService(user_id, ref_user_id);
      showToast("Theo dõi người dùng thành công", true);

      mainFollows = null;
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createUnfollowBtn(user_id, ref_user_id, btnWrapper) {
  let followingBtn = CrEl("button", "gray-btn pill-btn", btnWrapper, true);
  followingBtn._text("Đang theo dõi");

  followingBtn.onclick = async (e) => {
    if (!isClickable) return;
    isClickable = false;
    createFollowBtn(user_id, ref_user_id, btnWrapper);
    try {
      await unfollowUserService(user_id, ref_user_id);
      showToast("Hủy theo dõi người dùng thành công", true);

      mainFollows = null;
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

export function clearFollowers() {
  followers = null;
}
