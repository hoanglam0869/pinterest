import { loginService } from "../services.js";
import {
  Div,
  Img,
  CrEl,
  showLoading,
  showToast,
  hideLoading,
} from "../utils.js";
import { isValidEmail, isValidPassword, showMessage } from "../validate.js";

export function showLoginPopup(popupContent, callback = () => {}) {
  let popupHeader = CrEl("h3", "popup-header", popupContent);
  let img = Img("", popupHeader)._src("./img/pinterest.png");
  img.style.width = "40px";

  let title = Div("text-center mb-4", popupContent);
  CrEl("h2", "", title)._text("Welcome to my picture");

  let popupBody = Div("popup-body", popupContent);
  let emailCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let emailLabel = CrEl("label", "info-label", emailCon)._text("Email");
  emailLabel._attr({ for: "email" });
  let emailInput = CrEl("input", "info-input", emailCon);
  emailInput._attr({ placeholder: "Email", type: "email", name: "email" });
  let emailWarning = CrEl("span", "warning-text", emailCon);

  let passwordCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let passLabel = CrEl("label", "info-label", passwordCon)._text("Mật khẩu");
  passLabel._attr({ for: "password" });
  let passwordInput = CrEl("input", "info-input", passwordCon);
  passwordInput._attr({ placeholder: "Mật khẩu" });
  passwordInput._attr({ type: "password", name: "password" });
  let passwordWarning = CrEl("span", "warning-text", passwordCon);

  let popupFooter = Div("popup-footer", popupContent);
  let loginBtn = CrEl("button", "red-btn pill-btn", popupFooter);
  CrEl("i", "fa fa-sign-in-alt", loginBtn);
  CrEl("span", "ms-2", loginBtn)._text("Đăng nhập");

  loginBtn.onclick = async () => {
    let email = emailInput.value;
    let password = passwordInput.value;
    // kiểm tra hợp lệ
    let isValid = isValidEmail(email, emailWarning);
    isValid = isValid & isValidPassword(password, passwordWarning);
    // nếu không hợp lệ => dừng lại
    if (!isValid) return;

    emailWarning.innerText = "";
    passwordWarning.innerText = "";
    // đăng nhập
    try {
      showLoading(popupContent);
      let res = await loginService(email, password);

      showToast(res.data.message, true);
      callback(res.data.content);
    } catch (err) {
      hideLoading(err);
      if (!err.response) return;
      let { content, message } = err.response.data;
      if (content == "email") {
        showMessage(emailWarning, message);
      }
      if (content == "password") {
        showMessage(passwordWarning, message);
      }
    }
  };
  let inputs = [emailInput, passwordInput];
  inputs.forEach((input) => {
    input.onkeyup = (e) => {
      if (e.key == "Enter") loginBtn.click();
    };
  });
}
