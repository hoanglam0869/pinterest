import { getImageReactionsService } from "../services.js";
import { A, CrEl, Div, Img, hideLoading, showLoading } from "../utils.js";

let reactions = null;

export const showReactionPopup = async (
  popupContent,
  html,
  image_id,
  icons
) => {
  let popupHeader = CrEl("h3", "popup-header", popupContent);

  let reactionTotal = Div("reaction-total", popupHeader);
  Div("reaction-result", reactionTotal)._html(html[0]);
  Div("reaction-amount", reactionTotal)._html(html[1]);

  let popupBody = Div("popup-body", popupContent);
  await getImageReactions(image_id, popupBody);

  reactions.forEach((reaction) => {
    let { user_id, avatar, full_name } = reaction.user;

    let popupItem = Div("popup-item", popupBody);
    let aTag = A("d-flex align-items-center", popupItem);
    aTag._href(`./user.html?user_id=${user_id}`);

    let avatarCon = Div("avatar md-avatar me-2", aTag);
    if (avatar == "") {
      Div("no-avatar", avatarCon)._text(full_name.charAt(0).toUpperCase());
    } else {
      Img("w-100", avatarCon)._src(avatar);
    }
    CrEl("span", "", aTag)._text(full_name);

    let reactionCon = Div("reaction-user", popupItem);
    let index = icons.findIndex((icon) => icon.name == reaction.reaction_type);
    reactionCon.style.backgroundImage = `url(${icons[index].img})`;
  });
  popupBody.style.overflow = "hidden";
  if (popupBody.scrollHeight > popupBody.offsetHeight) {
    popupBody.style.overflow = "clip scroll";
  }
};

async function getImageReactions(image_id, popupBody) {
  if (reactions != null) return;
  try {
    showLoading(popupBody);
    let res = await getImageReactionsService(image_id);
    reactions = res.data.content;
    hideLoading();
  } catch (err) {
    hideLoading(err);
  }
}

export function clearReactions() {
  reactions = null;
}
