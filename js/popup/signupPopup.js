import { signupService } from "../services.js";
import {
  Div,
  Img,
  CrEl,
  hideLoading,
  showLoading,
  showToast,
} from "../utils.js";
import {
  isValidAge,
  isValidEmail,
  isValidName,
  isValidPassword,
  showMessage,
} from "../validate.js";

export function showSignupPopup(popupContent, callback = () => {}) {
  let popupHeader = CrEl("h3", "popup-header", popupContent);
  let img = Img("", popupHeader)._src("./img/pinterest.png");
  img.style.width = "40px";

  let title = Div("text-center mb-4", popupContent);
  CrEl("h2", "", title)._text("Welcome to my picture");
  CrEl("p", "", title)._text("Tìm những ý tưởng mới để thử");

  let popupBody = Div("popup-body", popupContent);
  let emailCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let emailLabel = CrEl("label", "info-label", emailCon)._text("Email");
  emailLabel._attr({ for: "email" });
  let emailInput = CrEl("input", "info-input", emailCon);
  emailInput._attr({ placeholder: "Email", type: "email", name: "email" });
  let emailWarning = CrEl("span", "warning-text", emailCon);

  let passwordCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let passLabel = CrEl("label", "info-label", passwordCon)._text("Mật khẩu");
  passLabel._attr({ for: "password" });
  let passwordInput = CrEl("input", "info-input", passwordCon);
  passwordInput._attr({ placeholder: "Mật khẩu" });
  passwordInput._attr({ type: "password", name: "password" });
  let passwordWarning = CrEl("span", "warning-text", passwordCon);

  let fullNameCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let fullNameLabel = CrEl("label", "info-label", fullNameCon)._text("Họ tên");
  fullNameLabel._attr({ for: "full-name" });
  let fullNameInput = CrEl("input", "info-input", fullNameCon);
  fullNameInput._attr({ placeholder: "Họ tên" });
  fullNameInput._attr({ type: "text", name: "full-name" });
  let fullNameWarning = CrEl("span", "warning-text", fullNameCon);

  let ageCon = Div("mb-2 mx-2 mx-md-4", popupBody);
  let ageLabel = CrEl("label", "info-label", ageCon)._text("Tuổi");
  ageLabel._attr({ for: "age" });
  let ageInput = CrEl("input", "info-input", ageCon);
  ageInput._attr({ placeholder: "Tuổi", type: "number", name: "age" });
  let ageWarning = CrEl("span", "warning-text", ageCon);

  let popupFooter = Div("popup-footer", popupContent);
  let signupBtn = CrEl("button", "red-btn pill-btn", popupFooter);
  CrEl("i", "fa fa-user-plus", signupBtn);
  CrEl("span", "ms-2", signupBtn)._text("Đăng ký");

  signupBtn.onclick = async () => {
    let email = emailInput.value;
    let password = passwordInput.value;
    let full_name = fullNameInput.value;
    let age = ageInput.value;
    // kiểm tra hợp lệ
    let isValid = isValidEmail(email, emailWarning);
    isValid = isValid & isValidPassword(password, passwordWarning);
    isValid = isValid & isValidName(full_name, fullNameWarning);
    isValid = isValid & isValidAge(age, ageWarning);
    // nếu không hợp lệ => dừng lại
    if (!isValid) return;
    emailWarning.innerText = "";
    passwordWarning.innerText = "";
    fullNameWarning.innerText = "";
    ageWarning.innerText = "";
    // đăng ký
    try {
      showLoading(popupContent);
      let res = await signupService(email, password, full_name, age);

      showToast(res.data.message, true);
      callback(res.data.content);
    } catch (err) {
      hideLoading(err);
      if (!err.response) return;
      let { content, message } = err.response.data;
      if (content == "email") {
        showMessage(emailWarning, message);
      }
    }
  };
  let inputs = [emailInput, passwordInput, fullNameInput, ageInput];
  inputs.forEach((input) => {
    input.onkeyup = (e) => {
      if (e.key == "Enter") signupBtn.click();
    };
  });
}
