import { deleteImageService, updateImageService } from "../services.js";
import {
  Button,
  CrEl,
  Div,
  Popup,
  hideLoading,
  showLoading,
  showToast,
} from "../utils.js";

export function showUpdateImagePopup(
  image,
  popupContent,
  popup,
  callback = () => {}
) {
  let { image_id, image_name, image_desc } = image;

  CrEl("h3", "popup-header", popupContent)._text("Chỉnh sửa Ghim");
  let popupBody = Div("popup-body", popupContent);

  let nameCon = Div("mb-3", popupBody);
  let nameLabel = CrEl("label", "info-label", nameCon);
  nameLabel._attr({ for: "image-name" })._text("Tiêu đề");

  let imgNameInput = CrEl("input", "info-input", nameCon);
  imgNameInput._attr({ placeholder: "Thêm tiêu đề" });
  imgNameInput._attr({ value: image_name, type: "text" });

  let descCon = Div("", popupBody);
  let descLabel = CrEl("label", "info-label", descCon);
  descLabel._attr({ for: "image-desc" })._text("Mô tả");

  let imgDescInput = CrEl("textarea", "info-input", descCon);
  imgDescInput._attr({
    placeholder: "Thêm mô tả chi tiết",
    name: "image-desc",
  });
  imgDescInput._attr({ cols: 30, rows: 8 });
  imgDescInput._html(image_desc);

  let popupFooter = Div("popup-footer", popupContent);
  let deleteImgBtn = Button("gray-btn pill-btn mx-2", popupFooter)._text("Xóa");
  let updateImgBtn = Button("red-btn pill-btn mx-2", popupFooter)._text("Lưu");

  updateImgBtn.onclick = async () => {
    let imgName = imgNameInput.value;
    let imgDesc = imgDescInput.value;
    try {
      showLoading(popupContent);
      await updateImageService(image_id, imgName, imgDesc);

      popup.hide();
      callback(imgName, imgDesc);
    } catch (err) {
      hideLoading();
    }
  };
  deleteImgBtn.onclick = () => {
    popup.hide();
    popup = Popup("delete-image-warning", (popupContent) => {
      showDeleteImagePopup(image_id, popupContent, popup);
    });
    popup.show();
  };
}

function showDeleteImagePopup(image_id, popupContent, popup) {
  CrEl("h3", "popup-header", popupContent)._text("Bạn có chắc không?");
  Div("popup-body", popupContent)._text(
    "Nếu bạn xóa thì Ghim này sẽ biến mất và những người đã lưu Ghim sẽ không thể xem được."
  );
  let popupFooter = Div("popup-footer", popupContent);

  let cancelBtn = Button("gray-btn pill-btn mx-2", popupFooter)._text("Hủy");
  let deleteImgBtn = Button("red-btn pill-btn mx-2", popupFooter)._text("Xóa");

  cancelBtn.onclick = () => popup.hide();
  deleteImgBtn.onclick = async () => {
    try {
      showLoading(popupContent);
      await deleteImageService(image_id);

      showToast("Xóa hình ảnh thành công", true);
      window.location.href = `./index.html`;
    } catch (err) {
      hideLoading();
    }
  };
}
