import { loginPopup } from "../setup/setup.js";
import { CrEl, Div, hideLoading, showLoading, showToast } from "../utils.js";

export function showUrlPopup(
  popupContent,
  user_id,
  popup,
  callback = () => {}
) {
  let popupHeader = CrEl("h3", "popup-header", popupContent);
  popupHeader._text("Lưu ý tưởng từ internet");
  CrEl("h5", "text-center", popupContent)._text("Sao chép đường dẫn hình ảnh");

  let popupBody = Div("popup-body d-flex align-items-end", popupContent);
  let urlInput = CrEl("input", "info-input mx-2 mx-md-4", popupBody);
  urlInput._attr({ type: "text", placeholder: "Nhập đường dẫn hình ảnh" });

  let popupFooter = Div("popup-footer", popupContent);
  let fetchImageBtn = CrEl("button", "red-btn pill-btn", popupFooter);
  CrEl("i", "fa fa-cloud-download-alt me-2", fetchImageBtn);
  CrEl("span", "", fetchImageBtn)._text("Lưu");

  fetchImageBtn.onclick = async () => {
    if (user_id == 0) {
      popup.hide();
      loginPopup.show();
      return;
    }
    let url = urlInput.value.trim();
    if (url == "") {
      showToast("Đường dẫn hình ảnh không được để trống", false);
      return;
    }
    let arr = url.split("/");
    let name = arr[arr.length - 1];
    try {
      showLoading(popupContent);
      let res = await fetch(url);
      let myBlob = await res.blob();
      const myFile = new File([myBlob], name, { type: myBlob.type });

      if (!myFile) return;

      const fr = new FileReader();
      fr.readAsDataURL(myFile);
      fr.onloadend = () => callback(myFile, fr.result);

      popup.hide();
      hideLoading();
    } catch (err) {
      hideLoading(err);
      showToast("Có lỗi", false);
    }
  };
  urlInput.onkeyup = (e) => {
    if (e.key == "Enter") {
      fetchImageBtn.click();
    }
  };
}
