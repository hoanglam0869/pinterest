import { loginPopup, setup } from "./setup/setup.js";
import { updateUserAvatarService, updateUserService } from "./services.js";
import { qS, hideLoading, showLoading, Div, Img } from "./utils.js";

let picture = qS("#picture");
let imageInput = qS(".image-input");
let userInfo = qS("#user-info");
let firstNameTxt = qS("#first-name");
let lastNameTxt = qS("#last-name");
let ageTxt = qS("#profile-age");
let resetBtn = qS("#reset-btn");
let saveBtn = qS("#save-btn");

let inputArr = [firstNameTxt, lastNameTxt, ageTxt];
let profile = [];
let isClickable = false;

setup((user, savedImages) => {
  if (user.user_id == 0) {
    qS("#upload-image").onclick = () => loginPopup.show();
    hideLoading();
    return;
  }
  let { user_id, full_name, age, avatar } = user;

  let div1 = Div("avatar xl-avatar", picture, true);
  if (avatar == "") {
    Div("no-avatar", div1)._text(full_name.charAt(0).toUpperCase());
  } else {
    Img("w-100", div1)._src(avatar);
  }
  qS("#upload-image").onclick = () => imageInput.click();

  handleImage(user_id);
  showInfo(full_name, age);
  handleChange();
  handleClick(user_id);
  hideLoading();
});

function handleImage(user_id) {
  imageInput.onchange = () => {
    const clickFile = imageInput.files[0];
    if (!clickFile) return;
    updateAvatar(user_id, clickFile);
  };
}

async function updateAvatar(user_id, file) {
  try {
    showLoading(picture);
    let res = await updateUserAvatarService(user_id, file);
    localStorage.setItem("token", res.data.content);

    hideLoading();
    window.location.reload();
  } catch (err) {
    hideLoading(err);
  }
}

function showInfo(full_name, age) {
  let arr = full_name.trim().split(" ");
  profile[0] = arr[0];
  firstNameTxt.value = profile[0];

  if (arr.length > 1) {
    let lastName = arr.map((n, i) => (i > 0 ? n : "")).join(" ");
    profile[1] = lastName.trim();
    lastNameTxt.value = profile[1];
  } else {
    profile[1] = "";
  }

  profile[2] = age;
  ageTxt.value = profile[2];
}

function handleChange() {
  inputArr.forEach((el) => {
    el.oninput = () => {
      setButtonsDisabled();
      for (let i = 0; i < inputArr.length; i++) {
        if (inputArr[i].value != profile[i]) {
          setButtonsAble();
          break;
        }
      }
    };
  });
}

function handleClick(user_id) {
  resetBtn.onclick = () => {
    if (!isClickable) return;
    inputArr.forEach((input, i) => (input.value = profile[i]));
    setButtonsDisabled();
  };
  saveBtn.onclick = async () => {
    if (!isClickable) return;

    let full_name = firstNameTxt.value + " " + lastNameTxt.value;
    let age = ageTxt.value;

    try {
      showLoading(userInfo);
      let res = await updateUserService(user_id, full_name, age);
      localStorage.setItem("token", res.data.content);
      setButtonsDisabled();

      showInfo(full_name, age);
      hideLoading();
    } catch (err) {
      hideLoading(err);
    }
  };
}

function setButtonsAble() {
  isClickable = true;
  saveBtn.className = "red-btn pill-btn mx-1";
  resetBtn.className = "gray-btn pill-btn mx-1";
}

function setButtonsDisabled() {
  isClickable = false;
  saveBtn.className = "disabled-btn pill-btn mx-1";
  resetBtn.className = "disabled-btn pill-btn mx-1";
}
