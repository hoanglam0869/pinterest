import { token } from "./setup/setup.js";

// const BASE_URL = "http://localhost:8081";
const BASE_URL = "http://178.128.31.62:4000";

export const signupService = (email, password, full_name, age) => {
  return axios({
    url: `${BASE_URL}/api/user/signup`,
    method: "POST",
    data: { email, password, full_name, age },
  });
};

export const loginService = (email, password) => {
  return axios({
    url: `${BASE_URL}/api/user/login`,
    method: "POST",
    data: { email, password },
  });
};

export const getUserService = () => {
  return axios({
    url: `${BASE_URL}/api/user/get-user`,
    method: "GET",
    headers: {
      token: token,
    },
  });
};

export const getAllUsersService = () => {
  return axios({
    url: `${BASE_URL}/api/user/get-all-user`,
    method: "GET",
  });
};

export const getUserByIdService = (user_id) => {
  return axios({
    url: `${BASE_URL}/api/user/get-user-by-id/${user_id}`,
    method: "GET",
  });
};

export const getImagesService = () => {
  return axios({
    url: `${BASE_URL}/api/image/get-image`,
    method: "GET",
  });
};

export const getImagesByNameService = (name) => {
  return axios({
    url: `${BASE_URL}/api/image/get-image-by-name?image_name=${name}`,
    method: "GET",
  });
};

export const hasSavedImageService = (user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/has-saved-image/${user_id}/${image_id}`,
    method: "GET",
    headers: {
      token: token,
    },
  });
};

export const getSavedImagesByUserService = (user_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-saved-image-by-user/${user_id}`,
    method: "GET",
  });
};

export const saveImageService = (user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/save-image/${user_id}/${image_id}`,
    method: "POST",
    headers: {
      token: token,
    },
  });
};

export const unsaveImageService = (user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/unsave-image/${user_id}/${image_id}`,
    method: "DELETE",
    headers: {
      token: token,
    },
  });
};

export const getImageDetailService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-image-detail/${image_id}`,
    method: "GET",
  });
};

export const getCommentsService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-comment/${image_id}`,
    method: "GET",
  });
};

export const saveCommentService = (content, user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/save-comment`,
    method: "POST",
    data: {
      content,
      user_id,
      image_id,
    },
    headers: {
      token: token,
    },
  });
};

export const updateCommentService = (comment_id, content) => {
  return axios({
    url: `${BASE_URL}/api/image/update-comment/${comment_id}`,
    method: "PUT",
    data: {
      content,
    },
    headers: {
      token: token,
    },
  });
};

export const deleteCommentService = (comment_id) => {
  return axios({
    url: `${BASE_URL}/api/image/delete-comment/${comment_id}`,
    method: "DELETE",
    headers: {
      token: token,
    },
  });
};

export const getCreatedImagesByUserService = (user_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-created-image-by-user/${user_id}`,
    method: "GET",
  });
};

export const uploadImageService = (file, image_name, image_desc, user_id) => {
  let bodyFormData = new FormData();
  bodyFormData.append("file", file);
  bodyFormData.append("image_name", image_name);
  bodyFormData.append("image_desc", image_desc);
  bodyFormData.append("user_id", user_id);

  return axios({
    url: `${BASE_URL}/api/image/upload-image`,
    method: "POST",
    headers: {
      token: token,
      "Content-Type": "multipart/form-data",
    },
    data: bodyFormData,
  });
};

export const updateUserService = (user_id, full_name, age) => {
  return axios({
    url: `${BASE_URL}/api/user/update-user/${user_id}`,
    method: "PUT",
    headers: {
      token: token,
    },
    data: {
      full_name,
      age,
    },
  });
};

export const updateUserAvatarService = (user_id, file) => {
  let bodyFormData = new FormData();
  bodyFormData.append("file", file);

  return axios({
    url: `${BASE_URL}/api/user/update-user-avatar/${user_id}`,
    method: "POST",
    headers: {
      token: token,
      "Content-Type": "multipart/form-data",
    },
    data: bodyFormData,
  });
};

export const createImageReactionService = (
  reaction_type,
  user_id,
  image_id
) => {
  return axios({
    url: `${BASE_URL}/api/image/create-image-reaction`,
    method: "POST",
    data: {
      reaction_type,
      user_id,
      image_id,
    },
    headers: {
      token: token,
    },
  });
};

export const getImageReactionByUserService = (user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-image-reaction-by-user/${user_id}/${image_id}`,
    method: "GET",
    headers: {
      token: token,
    },
  });
};

export const deleteImageReactionService = (user_id, image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/delete-image-reaction/${user_id}/${image_id}`,
    method: "DELETE",
    headers: {
      token: token,
    },
  });
};

export const getImageReactionsService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-image-reaction/${image_id}`,
    method: "GET",
  });
};

export const getImageReactionsAmountService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-image-reaction-amount/${image_id}`,
    method: "GET",
  });
};

export const getThreeMostImageReactionsService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/get-three-most-image-reaction/${image_id}`,
    method: "GET",
  });
};

export const updateImageReactionService = (
  reaction_type,
  user_id,
  image_id
) => {
  return axios({
    url: `${BASE_URL}/api/image/update-image-reaction`,
    method: "PUT",
    data: {
      reaction_type,
      user_id,
      image_id,
    },
    headers: {
      token: token,
    },
  });
};

export const getFollowersService = (following_id) => {
  return axios({
    url: `${BASE_URL}/api/user/get-follower/${following_id}`,
    method: "GET",
  });
};

export const getFollowersAmountService = (following_id) => {
  return axios({
    url: `${BASE_URL}/api/user/get-follower-amount/${following_id}`,
    method: "GET",
  });
};

export const getFollowingsService = (follower_id) => {
  return axios({
    url: `${BASE_URL}/api/user/get-following/${follower_id}`,
    method: "GET",
  });
};

export const getFollowingsAmountService = (follower_id) => {
  return axios({
    url: `${BASE_URL}/api/user/get-following-amount/${follower_id}`,
    method: "GET",
  });
};

export const hasFollowedUserService = (follower_id, following_id) => {
  return axios({
    url: `${BASE_URL}/api/user/has-followed-user/${follower_id}/${following_id}`,
    method: "GET",
    headers: {
      token: token,
    },
  });
};

export const followUserService = (follower_id, following_id) => {
  return axios({
    url: `${BASE_URL}/api/user/follow-user/${follower_id}/${following_id}`,
    method: "POST",
    headers: {
      token: token,
    },
  });
};

export const unfollowUserService = (follower_id, following_id) => {
  return axios({
    url: `${BASE_URL}/api/user/unfollow-user/${follower_id}/${following_id}`,
    method: "DELETE",
    headers: {
      token: token,
    },
  });
};

export const updateImageService = (image_id, image_name, image_desc) => {
  return axios({
    url: `${BASE_URL}/api/image/update-image/${image_id}`,
    method: "PUT",
    headers: {
      token: token,
    },
    data: {
      image_name,
      image_desc,
    },
  });
};

export const deleteImageService = (image_id) => {
  return axios({
    url: `${BASE_URL}/api/image/delete-image/${image_id}`,
    method: "DELETE",
    headers: {
      token: token,
    },
  });
};
