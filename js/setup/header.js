import { showImages } from "../controller.js";
import { getImagesByNameService } from "../services.js";
import {
  A,
  Div,
  CrEl,
  qS,
  urlIncludes,
  showLoading,
  hideLoading,
  DropDown,
} from "../utils.js";
import { loginPopup, signupPopup } from "./setup.js";

let homeClN = urlIncludes("index.html") ? "black-btn pill-btn" : "";
let crImgClN = urlIncludes("create-image.html") ? "black-btn pill-btn" : "";

export function createHeader(header, user, savedImages) {
  let nav = CrEl("nav", "navbar navbar-expand-lg bg-light", header, true);
  let container = Div("container-fluid", nav);

  let brand = A("nav-brand me-2", container)._href("./index.html");
  let brandIcon = Div("icon normal-icon p-2", brand);
  CrEl("img", "w-100", brandIcon).src = "./img/pinterest.png";

  A(homeClN, container)._href("./index.html")._text("Trang chủ");
  container.innerHTML += `<button
    class="navbar-toggler"
    type="button"
    data-bs-toggle="collapse"
    data-bs-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <span class="navbar-toggler-icon"></span>
  </button>`;

  let collapse = Div("collapse navbar-collapse text-center", container);
  collapse.id = "navbarSupportedContent";
  let crImgCon = Div("flex-center mb-2 mb-lg-0 ms-lg-4", collapse);
  let crImg = A(crImgClN, crImgCon)._href("./create-image.html")._text("Tạo");
  crImg._attr({ role: "button", "aria-expanded": false });

  searchBar(collapse, user.user_id, savedImages);
  if (user.user_id == 0) {
    createLogInAndSignUpBtn(collapse);
  } else {
    let { user_id, avatar, full_name, email } = user;
    createIcons(collapse, user_id, avatar, full_name, email);
  }
}

function searchBar(collapse, user_id, savedImages) {
  let searchCon = Div("flex-grow-1 mx-4 mb-2 mb-lg-0", collapse);

  let searchInput = CrEl("input", "info-input", searchCon);
  searchInput._attr({
    type: "search",
    placeholder: "Tìm kiếm",
    name: "search",
  });

  if (urlIncludes("index.html?image_name")) {
    let image_name = window.location.href.split("=")[1];
    searchInput.value = image_name;
  }
  searchInput.onchange = (e) => {
    search(e.target.value, user_id, savedImages);
  };
}

export async function search(image_name, user_id, savedImages) {
  if (image_name.trim() == "") return;
  if (urlIncludes("index.html")) {
    createLoadingImages();
    showLoading();
    try {
      let res = await getImagesByNameService(image_name);
      showImages(res.data.content, user_id, savedImages);
      hideLoading();
    } catch (err) {
      hideLoading(err);
    }
  } else {
    window.location.href = `./index.html?image_name=${image_name}`;
  }
}

function createLogInAndSignUpBtn(collapse) {
  let loginBtn = CrEl("button", "red-btn pill-btn mx-1", collapse);
  CrEl("i", "fa fa-sign-in-alt", loginBtn);
  CrEl("span", "ms-2", loginBtn)._text("Đăng nhập");
  loginBtn.onclick = () => loginPopup.show();

  let signupBtn = CrEl("button", "gray-btn pill-btn mx-1", collapse);
  CrEl("i", "fa fa-user-plus", signupBtn);
  CrEl("span", "ms-2", signupBtn)._text("Đăng ký");
  signupBtn.onclick = () => signupPopup.show();
}

function createIcons(collapse, user_id, avatar, full_name, email) {
  let iconCon = Div("mb-2 mb-lg-0 flex-center", collapse);
  let notificationIcon = Div("icon normal-icon", iconCon);
  CrEl("i", "fa fa-bell", notificationIcon);
  let chatIcon = Div("icon normal-icon", iconCon);
  CrEl("i", "fa fa-comment-dots", chatIcon);

  let userImg1 = A("", iconCon)._href(`./user.html?user_id=${user_id}`);
  let userIcon = Div("icon normal-icon", userImg1);
  let userAvatar = Div("avatar xs-avatar", userIcon);
  displayAvatar(full_name, avatar, userAvatar);

  let drd = Div("rotate-right-down", iconCon);
  let drdIcon = Div("icon small-icon", drd);
  CrEl("i", "fa fa-angle-down", drdIcon);

  DropDown({
    element: drd,
    action: "click",
    position: "drop-down-start",
    distX: 0,
    distY: 20,
    callback: (dropdownMenu) => {
      dropdownMenu.classList.add("text-start");

      let userImg2 = A("", dropdownMenu);
      userImg2._href(`./user.html?user_id=${user_id}`);
      let drdItem = Div("drop-down-item d-flex", userImg2);
      let userAvatar2 = Div("avatar md-avatar", drdItem);
      displayAvatar(full_name, avatar, userAvatar2);

      let userInfo = Div("ms-2", drdItem);
      CrEl("b", "", userInfo)._text(full_name);
      Div("", userInfo)._text(email);

      let logoutBtn = Div("drop-down-item", dropdownMenu);
      CrEl("i", "fa fa-sign-out-alt", logoutBtn);
      CrEl("span", "ms-2", logoutBtn)._text("Đăng xuất");
      logoutBtn.onclick = (e) => {
        e.stopPropagation();
        localStorage.removeItem("token");
        window.location.reload();
      };
    },
  });
}

function displayAvatar(full_name, avatar, container) {
  if (avatar == "") {
    Div("no-avatar", container)._text(full_name.charAt(0).toUpperCase());
  } else {
    CrEl("img", "w-100", container).src = avatar;
  }
}

export function createLoadingImages(count = 15) {
  let gallery = qS(".gallery");
  gallery.innerHTML = "";
  for (let i = 0; i < count; i++) {
    let galleryItem = Div("gallery-item", gallery);
    let figure = CrEl("figure", "", galleryItem);
    Div("img-load-bg", figure);
  }
}
