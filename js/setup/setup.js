import { showLoginPopup } from "../popup/loginPopup.js";
import { showSignupPopup } from "../popup/signupPopup.js";
import { getSavedImagesByUserService, getUserService } from "../services.js";
import { qS, Popup, hideLoading, showLoading, showToast } from "../utils.js";
import { createHeader } from "./header.js";

let header = qS("header");
export let loginPopup = Popup();
export let signupPopup = Popup();
export let token = localStorage.getItem("token");

export const setup = async (callback) => {
  // tạo popup đăng ký
  signupPopup = Popup("signup-popup", (popupContent) => {
    showSignupPopup(popupContent, (newToken) => {
      // popup ẩn sẽ xóa loading
      signupPopup.hide();
      // xử lý token
      token = newToken;
      localStorage.setItem("token", token);

      showLogin(callback);
    });
  });
  // tạo popup đăng nhập
  loginPopup = Popup("login-popup", (popupContent) => {
    showLoginPopup(popupContent, (newToken) => {
      // popup ẩn sẽ xóa loading
      loginPopup.hide();
      // xử lý token
      token = newToken;
      localStorage.setItem("token", token);

      showLogin(callback);
    });
  });
  await showLogin(callback);
};

async function showLogin(callback) {
  if (token == null) {
    createHeader(header, { user_id: 0 }, []);
    callback({ user_id: 0 }, []);
    return;
  }
  try {
    showLoading();
    // kiển tra người dùng
    let res = await getUserService();
    let user = res.data.content;
    // kiểm tra hình đã lưu hay chưa
    res = await getSavedImagesByUserService(user.user_id);
    let savedImages = res.data.content;

    createHeader(header, user, savedImages);
    hideLoading();
    callback(user, savedImages);
  } catch (err) {
    hideLoading(err);
    if (err.response?.data == "jwt expired") {
      showToast("Vui lòng đăng nhập lại", true);
      // xử lý token
      token = null;
      localStorage.removeItem("token");

      createHeader(header, { user_id: 0 }, []);
      callback({ user_id: 0 }, []);
    }
  }
}
