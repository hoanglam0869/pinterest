import { getCreatedImages, getSavedImages } from "./controller.js";
import { loginPopup, setup } from "./setup/setup.js";
import {
  followUserService,
  getFollowersAmountService,
  getFollowingsAmountService,
  getUserByIdService,
  hasFollowedUserService,
  unfollowUserService,
} from "./services.js";
import {
  qS,
  Tabs,
  CrEl,
  hideLoading,
  showToast,
  Popup,
  showLoading,
  Div,
  Img,
} from "./utils.js";
import { createLoadingImages } from "./setup/header.js";
import {
  clearFollowers,
  showFollowersPopup,
  showFollowingsPopup,
} from "./popup/followPopup.js";

let followWrapper = qS("#follow-wrapper");
let btnWrapper = qS("#btn-wrapper");
let userInfo = qS("#user-info");

let ref_user_id = window.location.href.split("=")[1];
let isClickable = true;
let popup = Popup();

createLoadingImages();

setup(async (user, savedImages) => {
  showLoading();
  let tabs = Tabs(1);
  let { user_id } = user;
  if (user_id == 0) {
    // hiện nút theo dõi
    let followBtn = CrEl("button", "gray-btn pill-btn", btnWrapper, true);
    followBtn._text("Theo dõi");
    followBtn.onclick = () => loginPopup.show();
    // thông tin người dùng
    await getUser();
    // xử lý theo dõi
    await handleFollow(user_id, true);
    hideLoading();
    // tải các hình đã tải và đã lưu
    getCreatedImages(ref_user_id, user_id, savedImages, tabs);
    getSavedImages(ref_user_id, user_id, savedImages, tabs);
    return;
  }
  if (user_id == ref_user_id) {
    // hiển thị hồ sơ
    displayProfile(user);
    // hiện nút chỉnh sửa hồ sơ
    let editProfile = CrEl("button", "gray-btn pill-btn", btnWrapper, true);
    editProfile._text("Chỉnh sửa hồ sơ");
    editProfile.onclick = () => {
      window.location.href = "./profile.html";
    };
  } else {
    await getUser();
    await showFollowBtn(user_id);
  }
  // xử lý theo dõi
  await handleFollow(user_id, true);
  hideLoading();
  // tải các hình đã tải và đã lưu
  getCreatedImages(ref_user_id, user_id, savedImages, tabs);
  getSavedImages(ref_user_id, user_id, savedImages, tabs);
});

async function getUser() {
  try {
    let res = await getUserByIdService(ref_user_id);
    displayProfile(res.data.content);
  } catch (err) {
    hideLoading(err);
  }
}

function displayProfile({ avatar, full_name, email }) {
  let div1 = Div("avatar xxl-avatar mx-auto", userInfo, true);
  if (avatar == "") {
    Div("no-avatar", div1)._text(full_name.charAt(0).toUpperCase());
  } else {
    Img("w-100", div1)._src(avatar);
  }
  let div2 = Div("d-flex flex-column align-items-center", userInfo);
  CrEl("h1", "mt-4", div2)._text(full_name);

  let emailCon = Div("text-secondary", div2);
  CrEl("i", "fab fa-pinterest", emailCon);
  CrEl("span", "ms-2", emailCon)._text(email);
}

async function handleFollow(user_id, isRefresh = false) {
  if (isRefresh) clearFollowers();
  try {
    let res = await getFollowersAmountService(ref_user_id);
    let flers = res.data.content;

    res = await getFollowingsAmountService(ref_user_id);
    let flings = res.data.content;

    if (flers == 0) {
      CrEl("div", "", followWrapper, true)._text("0 người theo dõi");
    } else {
      let totalFollower = CrEl("b", "c-pointer", followWrapper, true);
      totalFollower._text(flers + " người theo dõi");
      totalFollower.onclick = () => {
        popup = Popup("follower-popup", async (popupContent) => {
          let info = { amount: flers, ref_user_id, user_id };
          showFollowersPopup(popupContent, info, popup);
        });
        popup.show();
      };
    }
    CrEl("b", "mx-1", followWrapper)._text("·");
    if (flings == 0) {
      CrEl("div", "", followWrapper)._text("0 người đang theo dõi");
    } else {
      let totalFollowing = CrEl("b", "c-pointer", followWrapper);
      totalFollowing._text(flings + " người đang theo dõi");
      totalFollowing.onclick = () => {
        popup = Popup("following-popup", async (popupContent) => {
          let info = { amount: flings, ref_user_id, user_id };
          showFollowingsPopup(popupContent, info, popup);
        });
        popup.show();
      };
    }
  } catch (err) {
    hideLoading(err);
  }
}

async function showFollowBtn(user_id) {
  try {
    let res = await hasFollowedUserService(user_id, ref_user_id);
    let hasFollowedUser = res.data.content;

    if (hasFollowedUser) {
      createUnfollowBtn(user_id);
    } else {
      createFollowBtn(user_id);
    }
  } catch (err) {
    hideLoading(err);
  }
}

function createFollowBtn(user_id) {
  let followBtn = CrEl("button", "gray-btn pill-btn", btnWrapper, true);
  followBtn._text("Theo dõi");

  followBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;
    createUnfollowBtn(user_id);
    try {
      showLoading(followWrapper);
      await followUserService(user_id, ref_user_id);
      showToast("Theo dõi người dùng thành công", true);

      await handleFollow(user_id, true);
      hideLoading();
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}

function createUnfollowBtn(user_id) {
  let followingBtn = CrEl("button", "black-btn pill-btn", btnWrapper, true);
  followingBtn._text("Đang theo dõi");

  followingBtn.onclick = async () => {
    if (!isClickable) return;
    isClickable = false;
    createFollowBtn(user_id);
    try {
      showLoading(followWrapper);
      await unfollowUserService(user_id, ref_user_id);
      showToast("Hủy theo dõi người dùng thành công", true);

      await handleFollow(user_id, true);
      hideLoading();
      isClickable = true;
    } catch (err) {
      hideLoading(err);
    }
  };
}
