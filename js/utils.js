export const urlIncludes = (file) => {
  return window.location.href.includes(file);
};

export const showLoading = (container, styles = {}) => {
  let loading;
  if (container) {
    container.classList.add("position-relative");
    loading = Div("loading loading-element", container);
  } else {
    loading = Div("loading loading-page", document.body);
  }
  for (const key in styles) {
    loading.style[key] = styles[key];
  }
  Img("", loading)._src("./img/loading1.gif");
};

export const hideLoading = (message = "") => {
  let loading = qS(".loading");
  if (loading) {
    loading.parentElement.classList.remove("position-relative");
    loading.remove();
  }
  if (message.code == "ERR_NETWORK") {
    showToast("Lỗi kết nối", false);
  }
  /* if (message.code == "ERR_BAD_REQUEST") {
    showToast(message.response.data.message, false);
  } */
};

const toastClName = {
  success: "linear-gradient(to right, rgb(0, 176, 155), rgb(150, 201, 61))",
  fail: "linear-gradient(to right, rgb(255, 95, 109), rgb(255, 195, 113))",
};

export const showToast = (message, isSuccess) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: isSuccess ? toastClName.success : toastClName.fail,
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

export function hideWarning() {
  qSA(".warning-text").forEach((item) => {
    item.innerText = "";
  });
}

export const qS = (selector = "") => {
  let self = document.querySelector(selector);
  if (!self) return;

  self._text = function (text = "") {
    if (text) {
      self.innerText = text;
      self.classList.remove("load-bg");
      return self;
    }
    return self.innerText;
  };
  self._html = function (html = "") {
    if (html) {
      self.innerHTML = html;
      self.classList.remove("load-bg");
      return self;
    }
    return self.innerHTML;
  };
  self._css = function (styles = {}) {
    for (let key in styles) {
      self.style[key] = styles[key];
    }
    return self;
  };
  self._on = function (eventName = "", callback = () => {}) {
    self.addEventListener(eventName, callback);
    return self;
  };
  self._attr = (attributes = {}) => {
    for (let key in attributes) {
      self.setAttribute(key, attributes[key]);
    }
    return self;
  };
  return self;
};

export const qSA = (selector = "") => {
  return document.querySelectorAll(selector);
};

export const CrEl = (tagName = "", className, container, isEmpty = false) => {
  let self = document.createElement(tagName);
  if (className != "") self.className = className;

  if (isEmpty) container.innerHTML = "";
  container.appendChild(self);

  if (tagName != "a") {
    self._text = function (text = "") {
      if (text) {
        self.innerText = text;
        return self;
      }
      return self.innerText;
    };
    self._html = function (html = "") {
      if (html) {
        self.innerHTML = html;
        return self;
      }
      return self.innerHTML;
    };
  }
  self._css = function (styles = {}) {
    for (let key in styles) {
      self.style[key] = styles[key];
    }
    return self;
  };
  self._on = function (eventName = "", callback = () => {}) {
    self.addEventListener(eventName, callback);
    return self;
  };
  self._attr = (attributes = {}) => {
    for (let key in attributes) {
      self.setAttribute(key, attributes[key]);
    }
    return self;
  };
  return self;
};

export function Div(className = "", container, isEmpty = false) {
  let self = document.createElement("div");
  if (className != "") self.className = className;

  if (isEmpty) container.innerHTML = "";
  container.appendChild(self);

  self._text = (value = "") => {
    if (value) {
      self.innerText = value;
      return self;
    }
    return self.innerText;
  };
  self._html = function (html = "") {
    if (html) {
      self.innerHTML = html;
      return self;
    }
    return self.innerHTML;
  };
  return self;
}

export function A(className = "", container, isEmpty = false) {
  let self = document.createElement("a");
  if (className != "") self.className = className;

  if (isEmpty) container.innerHTML = "";
  container.appendChild(self);

  self._href = (value = "") => {
    self.setAttribute("href", value);
    return self;
  };
  self._text = (value = "") => {
    if (value) {
      self.innerText = value;
      return self;
    }
    return self.innerText;
  };
  self._attr = (attributes = {}) => {
    for (let key in attributes) {
      self.setAttribute(key, attributes[key]);
    }
    return self;
  };
  return self;
}

export function Img(className = "", container, isEmpty = false) {
  let self = document.createElement("img");
  if (className != "") self.className = className;

  if (isEmpty) container.innerHTML = "";
  container.appendChild(self);

  self._src = (value = "") => {
    if (value) {
      self.setAttribute("src", value);
      return self;
    }
    return self.getAttribute("src");
  };
  return self;
}

export function Button(className = "", container, isEmpty = false) {
  let self = document.createElement("button");
  if (className != "") self.className = className;

  if (isEmpty) container.innerHTML = "";
  container.appendChild(self);

  self._text = (value = "") => {
    if (value) {
      self.innerText = value;
      return self;
    }
    return self.innerText;
  };
  return self;
}

export const RotateArrow = (
  options = {
    selector,
    dir1: "down",
    dir2: "right",
  }
) => {
  let { selector, dir1, dir2 } = options;
  CrEl("i", `rotate-arrow fa fa-angle-${dir1}`, selector, true);

  return {
    setClick: (callback = () => {}) => {
      selector.onclick = () => {
        let isDir1 = false;

        let iTag = selector.querySelector(".rotate-arrow");
        if (iTag.classList.contains(`fa-angle-${dir1}`)) {
          isDir1 = true;
        }

        iTag.className = `rotate-arrow fa fa-angle-${isDir1 ? dir2 : dir1}`;
        callback(isDir1 ? dir2 : dir1);
      };
    },
    rotateToDirection: (dir, callback = () => {}) => {
      let className = `rotate-arrow fa fa-angle-${dir}`;
      selector.querySelector(".rotate-arrow").className = className;
      callback(dir);
    },
  };
};

export function DropDown(
  options = {
    element: HTMLElement,
    action: "click",
    position: "drop-down-center",
    distX: 0,
    distY: 0,
    callback: () => {},
  }
) {
  let { element, action, position, distX, distY, callback } = options;

  let dropdownMenu = document.createElement("div");
  dropdownMenu.className = "drop-down-menu";

  callback(dropdownMenu);
  let dropdown = {
    show: () => {
      dropdownMenu.style = "";
      document.body.appendChild(dropdownMenu);
      element.classList.add("drop-down-control");
      document.body.style.overflow = "auto";

      let rect1 = element.getBoundingClientRect();
      let { width, height, x, y } = rect1;

      let rect2 = dropdownMenu.getBoundingClientRect();
      let menuWidth = rect2.width;
      let menuHeight = rect2.height;

      let top = 0;
      let left = 0;
      if (position == null) {
        left = x + distX;
        top = y + distY;
      } else {
        switch (position) {
          case "drop-down-start":
            top = y + height + distY;
            left = x + width - menuWidth + distX;
            break;
          case "drop-down-center":
            top = y + height + distY;
            left = x + width / 2 - menuWidth / 2;
            break;
          case "drop-down-end":
            top = y + height + distY;
            left = x + distX;
            break;
          case "drop-up-start":
            top = y - menuHeight + distY;
            left = x + width - menuWidth + distX;
            break;
          case "drop-up-center":
            top = y - menuHeight + distY;
            left = x + width / 2 - menuWidth / 2;
            break;
          case "drop-up-end":
            top = y - menuHeight + distY;
            left = x + distX;
            break;
          case "drop-start-top":
            top = y + height - menuHeight + distY;
            left = x - menuWidth + distX;
            break;
          case "drop-start-center":
            top = y + height / 2 - menuHeight / 2;
            left = x - menuWidth + distX;
            break;
          case "drop-start-bottom":
            top = y + distY;
            left = x - menuWidth + distX;
            break;
          case "drop-end-top":
            top = y + height - menuHeight + distY;
            left = x + width + distX;
            break;
          case "drop-end-center":
            top = y + height / 2 - menuHeight / 2;
            left = x + width + distX;
            break;
          case "drop-end-bottom":
            top = y + distY;
            left = x + width + distX;
            break;
          default:
            top = y + distY;
            left = x + distX;
            break;
        }
      }
      if (top < 0) top = 0;
      if (left < 0) left = 0;
      dropdownMenu.style.top = top + "px";
      dropdownMenu.style.left = left + "px";
    },
    hide: () => {
      dropdownMenu.remove();
      element.classList.remove("drop-down-control");
      document.body.style.removeProperty("overflow");
    },
    setItem: (isFirstItem = false, callbackItem = () => {}) => {
      let dropdownItem = document.createElement("div");
      dropdownItem.className = "drop-down-item";
      if (isFirstItem) {
        dropdownMenu.prepend(dropdownItem);
      } else {
        dropdownMenu.appendChild(dropdownItem);
      }
      callbackItem(dropdownItem);
      return dropdown;
    },
  };
  switch (action) {
    case "click":
      element.onclick = (e) => {
        if (element.classList.contains("drop-down-control")) {
          dropdown.hide();
        } else {
          dropdown.show();
        }
      };
      break;
    case "hover":
      element.onmouseenter = () => dropdown.show();
      dropdownMenu.onmouseleave = () => dropdown.hide();
      break;
    default:
      break;
  }
  document.querySelectorAll("*").forEach((el) => {
    el.onmousedown = (e) => {
      let control = document.querySelector(".drop-down-control");
      if (control) {
        let menu = document.querySelector(".drop-down-menu");
        if (!control.contains(e.target) && !menu.contains(e.target)) {
          menu.remove();
          control.classList.remove("drop-down-control");
          document.body.style.removeProperty("overflow");
        }
      }
    };
  });
  return dropdown;
}

export function Tabs(defaultTabIndex) {
  let tabTitles = qSA(".tab-title");
  let tabContent = qS(".tab-content");
  let callbackObj = {};

  const showTab = (tabIndex) => {
    tabTitles.forEach((t) => t.classList.remove("active"));
    tabTitles[tabIndex].classList.add("active");
    callbackObj[`callback${tabIndex}`](tabContent);
  };

  tabTitles.forEach((tabTitle, i) => {
    tabTitle.onclick = () => showTab(i);
  });

  return {
    setClickOnTab: (tabIndex, callback) => {
      callbackObj[`callback${tabIndex}`] = callback;
      if (tabIndex == defaultTabIndex) {
        showTab(tabIndex);
      }
    },
  };
}

export function Popup(id = "", callback = () => {}) {
  let popupOverlay;
  let popupContent;
  let popupHide;
  let isPopupHideOnClickOutside = true;

  let popup = {
    show: () => {
      popupOverlay = document.createElement("div");
      popupOverlay.className = "popup-overlay";
      if (id) popupOverlay.id = id;
      popupOverlay.onclick = () => {
        if (!isPopupHideOnClickOutside) return;
        popupOverlay.remove();
        document.body.style.removeProperty("overflow");
      };
      document.body.appendChild(popupOverlay);
      document.body.style.overflow = "hidden";

      popupContent = document.createElement("div");
      popupContent.className = "popup-content";
      popupContent.onclick = (e) => {
        e.stopPropagation();
      };
      popupOverlay.appendChild(popupContent);

      popupHide = document.createElement("div");
      popupHide.className = "popup-hide icon";

      let hideIcon = document.createElement("i");
      hideIcon.className = "fa fa-times";

      popupHide.appendChild(hideIcon);
      popupContent.appendChild(popupHide);
      popupHide.onclick = () => popup.hide();

      callback(popupContent);
    },
    hide: () => {
      if (popupOverlay) {
        popupOverlay.remove();
        document.body.style.removeProperty("overflow");
      }
    },
    setPopupHideOnClickOutside: (value = true) => {
      isPopupHideOnClickOutside = value;
    },
  };
  return popup;
}
