export const showMessage = (element, message) => {
  element.innerText = message;
};

export const isValidEmail = (email, element) => {
  let regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regex.test(email)) {
    showMessage(element, "");
    return true;
  } else {
    showMessage(element, "Email không đúng định dạng");
    return false;
  }
};

export const isValidPassword = (password, element) => {
  let regex =
    /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–\[{}\]:;',?\/*~$^+=<>]).{8,20}$/g;
  if (regex.test(password)) {
    showMessage(element, "");
    return true;
  } else {
    showMessage(
      element,
      "Mật khẩu tối thiểu 8 ký tự (chứa ít nhất 1 số, 1 chữ cái thường, 1 chữ cái in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  }
};

export const isValidName = (name, element) => {
  var regex = /[^\s+]/g;
  if (regex.test(name)) {
    showMessage(element, "");
    return true;
  } else {
    showMessage(element, "Tên người dùng không được để trống");
    return false;
  }
};

export const isValidAge = (name, element) => {
  var regex = /^[1-9][0-9]*/g;
  if (regex.test(name)) {
    showMessage(element, "");
    return true;
  } else {
    showMessage(element, "Tuổi phải là số");
    return false;
  }
};
