import {
  createImageReactionService,
  followUserService,
  getAllUsersService,
  getImageDetailService,
  getUserByIdService,
  loginService,
  saveCommentService,
  saveImageService,
  signupService,
  updateUserAvatarService,
} from "../js/services.js";
import { qS, qSA } from "../js/utils.js";

let token = localStorage.getItem("token");

const uploadImageService = (file, image_name, image_desc, user_id) => {
  let bodyFormData = new FormData();
  bodyFormData.append("file", file);
  bodyFormData.append("image_name", image_name);
  bodyFormData.append("image_desc", image_desc);
  bodyFormData.append("user_id", user_id);

  return axios({
    url: `qS{BASE_URL}/api/image/upload-image`,
    method: "POST",
    headers: {
      token: token,
      "Content-Type": "multipart/form-data",
    },
    data: bodyFormData,
  });
};

let users = [];

async function getAllUsers() {
  let res = await getAllUsersService();

  let length = res.data.content.length;
  users = Array.from(Array(length), (_, index) => index + 1);
}

await getAllUsers();

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(array) {
  return array.sort(() => Math.random() - 0.5);
}

let peopleInVietnam = [
  {
    email: "annguyen@gmail.com",
    password: "P@ssw0rd1",
    fullName: "Nguyễn Văn An",
    age: 25,
    avatar: "./img/annguyen.jpg",
  },
  {
    email: "bichtran@yahoo.com",
    password: "Str0ngP@ss",
    fullName: "Trần Thị Bích",
    age: 30,
    avatar: "./img/bichtran.jpg",
  },
  {
    email: "congle@hotmail.com",
    password: "Secur3P@ssw",
    fullName: "Lê Văn Công",
    age: 28,
    avatar: "./img/congle.jpg",
  },
  {
    email: "duyenpham@outlook.com",
    password: "P@ssword4Ever",
    fullName: "Phạm Thị Duyên",
    age: 22,
    avatar: "./img/duyenpham.jpg",
  },
  {
    email: "emhoang@gmail.com",
    password: "Myp@ss12",
    fullName: "Hoàng Văn Em",
    age: 35,
    avatar: "./img/emhoang.jpg",
  },
  {
    email: "phuocvu@yahoo.com",
    password: "N3wP@ssword",
    fullName: "Vũ Thị Phước",
    age: 29,
    avatar: "./img/phuocvu.jpg",
  },
  {
    email: "giangdang@hotmail.com",
    password: "S@f3P@ssw",
    fullName: "Đặng Văn Giang",
    age: 31,
    avatar: "./img/giangdang.jpg",
  },
  {
    email: "hongtrinh@outlook.com",
    password: "L0ngP@ssw0rd",
    fullName: "Trịnh Thị Hồng",
    age: 27,
    avatar: "./img/hongtrinh.jpeg",
  },
  {
    email: "ichmai@gmail.com",
    password: "C0mpl3xP@ssw",
    fullName: "Mai Văn Ích",
    age: 26,
    avatar: "./img/ichmai.jpg",
  },
  {
    email: "khaly@yahoo.com",
    password: "S@f3P@ssw0rd",
    fullName: "Lý Thị Kha",
    age: 33,
    avatar: "./img/khaly.jpg",
  },
];

// ĐĂNG KÝ

async function signUp(count) {
  let { email, password, fullName, age } = peopleInVietnam[count];

  let res = await signupService(email, password, fullName, age);
  console.log(res);

  if (++count < peopleInVietnam.length) {
    signUp(count);
  } else {
    await getAllUsers();
  }
}

qS("#sign-up").onclick = () => {
  peopleInVietnam = shuffle(peopleInVietnam);
  signUp(0);
};

// ĐĂNG NHẬP

qS("#sign-in").onclick = async () => {
  let index = getRndInteger(0, peopleInVietnam.length - 1);
  let { email, password } = peopleInVietnam[index];

  let res = await loginService(email, password);
  console.log(res);

  let token = res.data.content;
  localStorage.setItem("token", token);
};

let images = [
  {
    image_name: "Trái cây",
    image_url: "./img/fruit.jpg",
    image_desc:
      "Hình ảnh về trái cây thường đẹp và ngon miệng. Chúng thường bao gồm các loại trái cây như táo, cam, nho, dâu, lựu và nhiều loại khác. Màu sắc tươi sáng của trái cây, cùng với hương thơm tự nhiên, tạo nên một cảm giác tươi mát và ngon miệng. Hình ảnh trái cây thường gợi lên hình ảnh của sự tươi ngon và là một phần quan trọng của chế độ ăn uống lành mạnh và cân đối. 🍏🍊🍇🍓🍍🍒🍎",
    user_id: 1,
    comments: [
      "Trái cây tươi mát luôn là một cách tuyệt vời để bắt đầu một ngày mới 🍏🌞.",
      "Hình ảnh trái cây có màu sắc rực rỡ thường là nguồn cảm hứng cho nghệ sĩ 🎨🍊.",
      "Khi nhìn vào hình ảnh trái cây, tôi cảm thấy như đang du lịch qua những vùng đất mới 🌍🍇.",
      "Trái cây là món quà của thiên nhiên cho sức khỏe của chúng ta, hãy trân trọng nó 🌱🍒.",
      "Không có gì thú vị hơn việc ngắm nhìn một bát trái cây đầy màu sắc 🍉🌼.",
      "Hình ảnh trái cây chín mọng là một lời nhắc nhở rằng thiên nhiên đầy bất ngờ và đẹp đẽ 🌈🍓.",
      "Mỗi trái cây trong bức tranh là một tác phẩm nghệ thuật tự nhiên 🍑🖼️.",
      "Trái cây luôn đem lại một cảm giác tự nhiên và sự cân bằng cho bữa ăn của tôi 🥭🥦.",
      "Hình ảnh trái cây là một biểu tượng của sự thịnh vượng và hạnh phúc trong cuộc sống 🌟🍍.",
      "Nhìn vào hình ảnh trái cây, tôi thấy mình bị quyến rũ bởi vẻ đẹp và hương vị của thiên nhiên 🌺🍎.",
    ],
  },
  {
    image_name: "Phòng khách",
    image_url: "./img/cambg_7.jpg",
    image_desc:
      "Hình ảnh phòng khách thường thể hiện một không gian nội thất trong ngôi nhà, với các bộ ghế, bàn và các vật trang trí. Phòng khách thường được thiết kế để tạo môi trường thoải mái cho gặp gỡ và thư giãn. Màu sắc và phong cách trang trí trong phòng khách có thể đa dạng, từ hiện đại và sang trọng đến ấm cúng và truyền thống. Phòng khách thường là nơi tập trung của gia đình, nơi mọi người có thể nói chuyện, xem TV, đọc sách hoặc tổ chức các hoạt động xã hội. 🛋️📺📚👨‍👩‍👧‍👦",
    user_id: 1,
    comments: [
      "Phòng khách này trông rất ấm cúng và thoải mái, đúng không? 🛋️🏡",
      "Tôi thích cách mà màu sắc và thiết kế của phòng khách này tạo cảm giác thư giãn 🎨🛋️.",
      "Phòng khách này là nơi tuyệt vời để tụ tập với gia đình và bạn bè 🤗🛋️.",
      "Màu sắc tươi sáng trong phòng khách làm tôi cảm thấy tinh thần hơn 🌈🛋️.",
      "Không gian này thật thoải mái, tôi muốn nằm xuống ghế và thư giãn ngay lập tức 😌🛋️.",
      "Phòng khách này là một nơi tuyệt vời để thả lỏng sau một ngày làm việc căng thẳng 🍃🛋️.",
      "Các chi tiết trang trí trong phòng khách này tạo nên không gian ấm cúng và phong cách 🪴🛋️.",
      "Tôi yêu cầu sắc đẹp của phòng khách này, nó là nơi hoàn hảo để tận hưởng thời gian với gia đình 🌟🛋️.",
      "Mùi thơm của cây cỏ và hoa trong phòng khách tạo cảm giác thư giãn và hạnh phúc 🌸🛋️.",
      "Phòng khách này thật sự là một nơi đáng sống và thư giãn 🏡🛋️.",
    ],
  },
  {
    image_name: "The Flash",
    image_url: "./img/flash.jpeg",
    image_desc:
      "The Flash là một siêu anh hùng nổi tiếng trong vũ trụ DC Comics. Nhân vật chính của The Flash là Barry Allen, một chuyên viên phân tích tốc độ trong cảnh sát Central City. Sau một tai nạn với một cái bể chứa các sản phẩm hóa học, Barry Allen trở thành người có khả năng chạy với vận tốc siêu phàm.\n\nHình ảnh The Flash thường thấy anh mặc một bộ đồ màu đỏ với một chiếc áo choàng màu vàng. Anh có mặt nạ che kín khuôn mặt, chỉ để lộ đôi mắt màu xanh lá cây. Khả năng chạy siêu nhanh của The Flash là sức mạnh chính, giúp anh ta đối phó với tội phạm và tham gia vào các cuộc chiến siêu anh hùng trong vũ trụ DC. The Flash thường được mô tả như một biểu tượng của tốc độ và tinh thần tự thúc đẩy anh ta trong việc bảo vệ thành phố khỏi hiểm họa. ⚡🏃‍♂️💥",
    user_id: 2,
    comments: [
      "The Flash là siêu anh hùng với sức mạnh siêu tốc độ, luôn sẵn sàng để cứu người dưới bất kỳ hoàn cảnh nào ⚡❤️.",
      "Tốc độ và sức mạnh của The Flash là điều khiến anh trở thành một biểu tượng trong vũ trụ siêu anh hùng của DC Comics 🏃💨.",
      "Khả năng sử dụng Speed Force khiến The Flash trở thành một trong những nhân vật mạnh nhất trong DC Universe 🌌💪.",
      "Hình ảnh The Flash luôn là một minh chứng cho sự kiên nhẫn và quyết tâm vượt qua mọi khó khăn ⚡🤛.",
      "The Flash không chỉ là siêu anh hùng mà còn là một người bạn đáng tin cậy, luôn ở bên để bảo vệ Central City 🌆🦸.",
      "Tốc độ và sự hài hước của The Flash khiến anh trở thành một trong những nhân vật yêu thích trong thế giới siêu anh hùng 🤣❤️.",
      "Mọi khi The Flash xuất hiện, bạn biết rằng sẽ có một cuộc chiến hành động đầy kịch tính và hấp dẫn ⚡🔥.",
      "Tính cách tương phản giữa tốc độ cực nhanh và tính cẩn trọng của Barry Allen tạo nên một nhân vật phức tạp và thú vị 🤔⚡.",
      "Hình ảnh The Flash chứng tỏ rằng một người bình thường có thể trở thành một siêu anh hùng nếu họ tin vào bản thân mình và nỗ lực hết mình ⚡💥.",
      "The Flash là biểu tượng của hi vọng, và sự hiện diện của anh luôn đem lại niềm tin trong tương lai 🌟⚡.",
    ],
  },
  {
    image_name: "Giáng sinh",
    image_url: "./img/cambg_3.jpg",
    image_desc:
      "Hình ảnh về Giáng sinh thường đầy màu sắc và tươi vui. Những hình ảnh này thường bao gồm một cây thông Noel được trang trí bằng đèn sáng, bóng cầu, và các trang sức lấp lánh. Những món quà được đặt dưới cây để chờ đêm Giáng sinh. Người ta thường thấy hình ảnh của ông già Noel, người đeo bộ áo đỏ, mũ đỏ và túi quà lớn để phát quà cho trẻ em vào đêm Giáng sinh.\n\nHình ảnh về Giáng sinh cũng bao gồm tuyết rơi, làm bồng bềnh những bông tuyết trắng tinh khôi. Khung cảnh ấm áp của gia đình và bạn bè quây quần bên bữa tối Giáng sinh cùng với lửa trong lò sưởi tạo ra một cảm giác ấm cúng và đoàn tụ.\n\nNgoài ra, hình ảnh về Giáng sinh cũng bao gồm những bữa tiệc thực đơn đặc biệt với thịt gà, hạt dẻ và bánh quả dứa. Tất cả những hình ảnh này tạo nên bầu không khí ấm áp và vui vẻ của mùa lễ Giáng sinh. 🎄🎅🎁❄️🕯️🌟",
    user_id: 2,
    comments: [
      "Mùa Giáng Sinh là thời gian thú vị để tận hưởng gia đình và bạn bè 🎄🎅.",
      "Không gian với ánh sáng đèn đẹp lung linh làm cho mùa giáng sinh trở nên phép màu 🌟❄️.",
      "Hình ảnh cây thông trang trí và quà tặng chờ đợi dưới cây làm tôi cảm thấy hạnh phúc và háo hức 🎁😊.",
      "Giáng Sinh là thời gian của yêu thương và lòng nhân ái, khi mọi người kết nối và chia sẻ niềm vui 🤗❤️.",
      "Ngắm nhìn hình ảnh tuyết rơi là như thấy thiên đàng đổ xuống trái đất ❄️☃️.",
      "Không gian với mùi thơm của bánh quế và quả cam làm tôi nhớ về hương vị ấm áp của mùa đông 🍊🍰.",
      "Cùng nhau hát bài ca Giáng Sinh và chia sẻ bữa tối đặc biệt làm cho mùa lễ hội trở nên đáng nhớ 🎶🍴.",
      "Hình ảnh người dân trang trí những ngôi nhà bằng đèn và trang sức giáng sinh thực sự tạo nên không gian ấm áp 🏘️🌟.",
      "Chơi với tuyết và xây người tuyết là một phần không thể thiếu của niềm vui Giáng Sinh ❆⛄.",
      "Mùa Giáng Sinh là thời gian cho những ước mơ trở thành hiện thực và cho hy vọng vào tương lai 🌠✨.",
    ],
  },
  {
    image_name: "Hoa hướng dương",
    image_url: "./img/sunflower.jpg",
    image_desc:
      'Hoa hướng dương là một loài hoa đặc trưng với bông hoa to và màu vàng tươi, thường có hình dạng tương tự như mặt trời với những cánh hoa sáng rực bao quanh một trung tâm nổi bật. Loài hoa này thường là biểu tượng của sự tươi vui, niềm hy vọng và sự lạc quan.\n\nHoa hướng dương thường được tìm thấy trong vùng nhiệt đới và mùa hè, và chúng thường quay theo hướng của mặt trời, từ đó tên gọi "hướng dương." Cánh hoa màu vàng tươi sáng của chúng tỏa sáng dưới ánh nắng mặt trời, tạo nên một hình ảnh rạng ngời và đầy năng lượng.\n\nHoa hướng dương thường được trồng trong vườn hoa hoặc trên các cánh đồng và thường được ưa chuộng vì vẻ đẹp và ý nghĩa tinh thần tích cực mà chúng mang lại. Hình ảnh của hoa hướng dương thường kích thích cảm giác niềm vui và sự tự do, cũng như khả năng tự quay về hướng ánh sáng, tượng trưng cho lòng lạc quan và hy vọng trong cuộc sống. 🌻🌞🌼',
    user_id: 3,
    comments: [
      "Hoa hướng dương luôn đem lại nụ cười và tia nắng rạng rỡ vào mọi ngày 🌻😊.",
      "Hình ảnh hoa hướng dương tỏa sáng như mặt trời, đánh thức tâm hồn và đem lại niềm hy vọng 🌞🌻.",
      "Màu vàng tươi sáng của hoa hướng dương khiến tôi cảm thấy ấm áp và vui vẻ 🌼🌞.",
      "Hoa hướng dương biểu trưng cho sự lạc quan và sự lớn lên, như tia nắng trong mỗi bức tranh 🌞🌱.",
      "Hình ảnh hoa hướng dương nở rộ là minh chứng cho sức mạnh của thiên nhiên và khả năng tái sinh 🌼🌿.",
      "Ngắm nhìn hoa hướng dương là một cách tuyệt vời để tìm kiếm sự đẹp và đơn giản trong cuộc sống 🌻❤️.",
      "Hoa hướng dương giống như một nụ cười tự nhiên của thiên nhiên, luôn tươi sáng và đáng yêu 🌞🙂.",
      "Sự kiên nhẫn của hoa hướng dương theo ánh mặt trời là nguồn cảm hứng về sự kiên trì và đổi mới 🌻🌄.",
      "Mùi thơm dịu dàng của hoa hướng dương làm cho mùi khắp nơi trở nên thơm ngát và tươi mát 🌻🌬️.",
      "Hoa hướng dương như một lời nhắc nhở rằng, giống như mặt trời, chúng ta cũng có thể tỏa sáng trong cuộc sống của mình 🌞✨.",
    ],
  },
  {
    image_name: "Bức tường",
    image_url: "./img/cambg_4.jpg",
    image_desc:
      "Bức tường là một phần không thể thiếu trong kiến trúc và không gian xây dựng. Đây là một thành phần quan trọng trong việc xác định sự chia cắt và định hình không gian. Bức tường có thể được làm từ nhiều loại vật liệu khác nhau, bao gồm gạch, bê tông, gỗ, và nhiều loại vật liệu xây dựng khác.\n\nHình ảnh của bức tường thường thể hiện sự đa dạng trong thiết kế và trang trí. Các bức tường có thể được sơn màu, tráng men, hoặc trang trí bằng tranh hoặc tấm ốp. Bức tường cũng có thể có các cửa sổ, cửa đi, hoặc vật liệu xây dựng khác để tạo sự kết nối với các phòng khác hoặc môi trường bên ngoài.\n\nBức tường đóng vai trò quan trọng trong việc tạo nên không gian riêng tư và bảo vệ khỏi yếu tố bên ngoài. Đồng thời, chúng cũng có thể trở thành bề mặt để trình bày nghệ thuật hoặc thông điệp thẩm mỹ. Hình ảnh của bức tường trong kiến trúc thường thể hiện tính chắc chắn và cấu trúc của môi trường xây dựng. 🏗️🎨🚪🖼️",
    user_id: 3,
    comments: [
      "Bức tường này có màu sắc rất bắt mắt và tạo nên không gian sống phong cách 🎨🏡.",
      "Tôi thích cách bức tường này làm cho căn phòng trở nên rộng lớn hơn và thoải mái hơn 🏢😌.",
      "Những chi tiết trên bức tường này khiến tôi nghĩ đến lịch sử và văn hóa độc đáo 🏰📜.",
      "Màu sắc và hoa văn trên bức tường này tạo ra một không gian ấm cúng và thú vị 🎉🎆.",
      "Bức tường này là nơi tôi thường trưng bày các tác phẩm nghệ thuật yêu thích của mình 🖼️🎨.",
      "Cảm giác khi ngồi bên cạnh bức tường này luôn làm tôi cảm thấy bình yên và tĩnh lặng 🧘‍♂️🏡.",
      "Những họa tiết trên bức tường này mang lại sự phong cách và cá nhân hóa cho không gian 🌟🏠.",
      "Tôi thường tạo những kí ức đáng nhớ trước bức tường này, bằng cách treo hình ảnh và tranh của gia đình 📸👨‍👩‍👧‍👦.",
      "Bức tường này là bảng thư viện của tôi, nơi tôi để sách và tư duy tự do 📚📖.",
      "Tôi luôn tìm cách thúc đẩy sự sáng tạo bằng cách viết lên bức tường này 🖋️📝.",
    ],
  },
  {
    image_name: "Cà phê",
    image_url: "./img/coffee.jpg",
    image_desc:
      "Hình ảnh cà phê thường là một tách cà phê nóng, đậm đà, và thơm phức, thường đặt trên một mặt bàn hoặc góc tường của một quán cà phê hoặc phòng khách. Cà phê thường có màu nâu đậm và thường được đựng trong các cốc hoặc ly pha lê hoặc gốm.\n\nHương thơm của cà phê có thể làm cho người ta tỉnh táo và thư giãn cùng mùi cà phê nồng nàn. Đôi khi, hình ảnh này còn bao gồm những hạt cà phê xanh mướt hoặc túi cà phê được rang sẵn, tạo nên mùi thơm đặc trưng và hấp dẫn.\n\nCà phê thường là một biểu tượng của năng lượng và sự tỉnh táo, là nguồn cảm hứng cho những người yêu thích hương vị và hương thơm của nó. Hình ảnh cà phê thường tạo nên một không gian ấm cúng để nghỉ ngơi, làm việc, hoặc thư giãn. ☕🌿🏠👃😌",
    user_id: 4,
    comments: [
      "Mùi cà phê sưởi ấm không gian và làm cho tôi tỉnh táo vào mỗi buổi sáng ☕️🌞.",
      "Hình ảnh cốc cà phê đầy bọt là một nghệ thuật thưởng thức tuyệt vời 🥰☕️.",
      "Không gian với hương cà phê thơm ngon làm cho mọi người thèm khát và sẵn sàng để làm việc 💪☕️.",
      "Cà phê là nguồn cảm hứng lớn cho sự sáng tạo, giúp tôi tập trung và làm việc hiệu quả 🎨☕️.",
      "Hình ảnh cà phê và sách là cách hoàn hảo để thư giãn và tận hưởng thời gian riêng tư 📚☕️.",
      "Một cốc cà phê nóng là sự khởi đầu hoàn hảo cho mỗi ngày, đem lại năng lượng và sự hứng thú 🌅☕️.",
      "Cảm giác khi cầm ly cà phê ấm áp là như một vòng tay của tình yêu và sự an ủi 🤗☕️.",
      "Mùi cà phê pha máy buổi sáng là một gợi nhắc về một ngày tuyệt vời trước mắt 🌞☕️.",
      "Cà phê không chỉ là một đồ uống, mà còn là một phần của cuộc sống và văn hóa 🌍☕️.",
      "Hình ảnh cà phê đưa tôi vào thế giới của niềm đam mê và thưởng thức hương vị 🌟☕️.",
    ],
  },
  {
    image_name: "Mèo cưng",
    image_url: "./img/cute_cat.jpg",
    image_desc:
      "Hình ảnh mèo cưng thường là một hình ảnh đáng yêu và đáng yêu về một chú mèo, một thành viên thân thương trong gia đình. Mèo cưng thường có bộ lông mềm mịn, đôi mắt sáng lấp lánh và đôi tai nhỏ xinh. Họ có thể được thấy khi chơi đùa, thư giãn trên sofa, hoặc vuốt ve với chủ nhân của họ.\n\nHình ảnh mèo cưng thường thể hiện tính tình tò mò, đáng yêu và thân thiện của họ. Chúng có thể gây cười với những động tác nghịch ngợm hoặc cách họ giơ đuôi lên khi họ hạnh phúc. Mèo cưng thường là nguồn cảm hứng vô giá cho những người yêu thú cưng và là bạn đồng hành đáng yêu trong cuộc sống hàng ngày.\n\nHình ảnh mèo cưng thường thể hiện mối tình yêu và sự quan tâm từ phía người chủ, và chúng thường là một phần không thể thiếu trong nhiều gia đình, đem lại sự ấm áp và niềm vui trong cuộc sống hàng ngày. 🐱💕😺🏡🐾",
    user_id: 4,
    comments: [
      "Mèo cưng luôn là nguồn ngạc nhiên với sự đáng yêu và sự đáng yêu vô tận của họ 😻❤️.",
      "Có gì đáng yêu hơn khi mèo nhỏ quấn quýt và tỏ ra thân thiện? 😺🥰",
      "Hình ảnh mèo cưng thường làm tôi mỉm cười và cảm thấy ấm áp bên trong 🐾😊.",
      "Mèo cưng là những người bạn đáng yêu và đồng hành tốt nhất trong cuộc sống của chúng ta 🐱👫.",
      "Ánh mắt của mèo có khả năng tạo kết nối tinh tế và sâu sắc, làm cho chúng trở thành thành viên đặc biệt trong gia đình 🐈👪.",
      "Xem mèo chơi và nghịch ngợm thường làm tôi cảm thấy vui vẻ và trẻ trung 🎉🐾.",
      "Mèo cưng có khả năng thư giãn và giảm căng thẳng, họ là những bác sĩ tinh thần tuyệt vời 🌼🐱.",
      "Khi tôi nhìn vào hình ảnh mèo cưng, tôi luôn cảm thấy yên bình và an lành 🏡😌.",
      "Mèo là những người bạn không bao giờ phản bội và luôn sẵn sàng để chia sẻ tình yêu 🐾❤️.",
      "Hình ảnh mèo cưng là lời gợi nhắc rằng đôi khi đơn giản là tốt nhất, và yêu thú cưng là điều đáng trân trọng 🧡🐱.",
    ],
  },
  {
    image_name: "Thị trấn ven biển",
    image_url: "./img/beach_town.jpg",
    image_desc:
      "Hình ảnh thị trấn ven biển thường là một phong cảnh đáng yêu với những ngôi nhà gần biển, những con đường nhỏ dẫn ra bãi biển cát trắng mịn, và một biển xanh biếc kéo dài tới chân trời. Thị trấn ven biển thường tạo ra một không gian yên bình và thư giãn, với không khí mặn mà của biển, tiếng sóng biển vỗ nhẹ và gió biển mát lành.\n\nNhững hình ảnh này thường bao gồm những chiếc thuyền nhỏ neo đậy tại bến cảng, các quán cà phê ven biển với những ghế dựa dưới bóng dừa, và hoạt động như lướt sóng, bơi biển, hoặc tắm nắng trên bãi biển. Cảnh quan ven biển thường đa dạng, từ bãi cát dài và bãi đá đáng kỳ vĩ đến bãi biển với những dãy núi xanh rì và cây cối nhiệt đới.\n\nHình ảnh thị trấn ven biển thường thúc đẩy cảm giác thư giãn và thoải mái, tạo nên một môi trường lý tưởng cho người ta để thư giãn, tận hưởng tự nhiên, và tránh xa khỏi cuộc sống hối hả của thành phố. 🌊🌴🏖️🌞🏠",
    user_id: 5,
    comments: [
      "Thị trấn ven biển là nơi tuyệt vời để thoát khỏi cuộc sống đô thị ồn ào và thư giãn 🏖️🌅.",
      "Cảm giác sóng biển lặng lẽ và cát trắng dài làm cho tôi cảm thấy yên bình và tĩnh lặng 🌊🌾.",
      "Hình ảnh thị trấn ven biển với bãi cát và biển xanh làm tôi nhớ về kì nghỉ tuyệt vời và niềm vui 🌞🏄.",
      "Thị trấn ven biển là nơi lý tưởng để tham gia các hoạt động ngoại trời và thư giãn cùng gia đình và bạn bè 🚣🍹.",
      "Ngắm nhìn bình minh và hoàng hôn tại thị trấn ven biển là trải nghiệm tuyệt vời, tôi thích nhất 🌇🌆.",
      "Hình ảnh những con thuyền cá lặng lẽ trên biển tạo nên một cảm giác truyền thống và yên tĩnh 🚤⚓.",
      "Thị trấn ven biển với quán cà phê và nhà hàng trên bãi biển là nơi lý tưởng để thưởng thức đồ ăn ngon và cảnh quan đẹp 🍔🍷.",
      "Bãi biển với cát mịn và nước biển trong xanh là nơi hoàn hảo để tận hưởng bữa tắm nắng và biển 🌴🌞.",
      "Thị trấn ven biển luôn tạo cho tôi cảm giác như đang sống trong một bức tranh hoàn hảo của thiên nhiên 🖼️🏖️.",
      "Khi tôi nhìn thấy hình ảnh thị trấn ven biển, tôi cảm thấy như đang tiến vào một thế giới yên bình và thư giãn 🌊🏝️.",
    ],
  },
  {
    image_name: "Thuyền buồm",
    image_url: "./img/midjourney-after.png",
    image_desc:
      "Hình ảnh thuyền buồm thường là một tác phẩm nghệ thuật nằm trên biển xanh mênh mông, với một chiếc thuyền buồm cao cánh độc đáo. Thuyền buồm thường có bộ buồm to lớn, vươn ra ngang trời, và cánh buồm thường được bật cao, bắt gió để đẩy thuyền chuyển động. Bầu trời xanh và biển cả rộng lớn là phần của bức tranh đẹp mắt này.\n\nHình ảnh này thường tạo ra một cảm giác của sự tự do và sự phiêu lưu, với thuyền buồm trôi dạt trên sóng biển mênh mông. Ngọn gió đẩy thuyền đi với tốc độ, tạo nên một trải nghiệm tự nhiên đầy hứng thú. Thuyền buồm là biểu tượng của sự đoàn kết và tương tác giữa con người và thiên nhiên, cũng như niềm đam mê khám phá các vùng biển xa xôi.\n\nHình ảnh thuyền buồm thường kích thích sự mơ mộng và mong muốn khám phá những vùng biển mới, và nó thường được yêu thích trong việc tạo ra cảm giác bình yên và thoải mái. 🌊⛵🌞🌴🌬️",
    user_id: 5,
    comments: [
      "Hình ảnh thuyền buồm giữa biển mênh mông là biểu tượng của tự do và phiêu lưu 🌊⛵.",
      "Thuyền buồm trên sóng biển đưa tôi vào cuộc hành trình đầy hứng thú và khám phá 🌅🌊.",
      "Khám phá thế giới qua hình ảnh thuyền buồm là một cơ hội thú vị để tìm hiểu về địa điểm mới và văn hóa khác nhau 🗺️🏝️.",
      "Hình ảnh bầu trời xanh và biển bát ngát luôn làm tôi thấy tĩnh lặng và thư thái 🌞🌊.",
      "Thuyền buồm là một cách tuyệt vời để tận hưởng thiên nhiên và cảm nhận sức mạnh của gió 🍃⛵.",
      "Tôi luôn cảm thấy kích thích và sẵn sàng khám phá khi thấy hình ảnh thuyền buồm trên biển xanh 🤩⛵.",
      "Cuộc sống trên biển với hình ảnh thuyền buồm mang lại sự giản dị và yên bình 🌊⛱️.",
      "Thuyền buồm là một biểu tượng của tình bạn, nơi mọi người làm việc cùng nhau để du hành xa xôi 🙌⛵.",
      "Hình ảnh thuyền buồm trên sóng biển xanh là niềm đam mê và niềm mơ ước của nhiều người ⛵🌊.",
      "Tôi luôn thấy thuyền buồm là một phần của trái tim và linh hồn, là niềm đam mê của cuộc đời 🌊❤️.",
    ],
  },
  {
    image_name: "Kem",
    image_url: "./img/ice_cream.png",
    image_desc:
      "Hình ảnh kem thường là một cốc hoặc viên kem màu sắc ngon mắt, với lớp kem mượt mà trên mặt và thường được trang trí bằng số lượng lớn những loại topping, như sô cô la nước, hạt dẻ, trái cây tươi, hoặc bánh quy băm nhỏ. Kem thường được đặt trên một chiếc mát xa hoặc tách đá, và thường đi kèm với một chiếc thìa hoặc ống hút.\n\nHình ảnh kem thường kích thích vị giác và thị giác, với hương vị ngọt ngào và mát lạnh của kem tan chảy trong miệng. Không chỉ ngon mắt, mà còn thơm ngon và bổ dưỡng. Kem là biểu tượng của sự thưởng thức và thỏa mãn, là món ăn tráng miệng lý tưởng cho những ngày nóng và cả trong những dịp lễ kỷ niệm.\n\nHình ảnh kem thường gợi lên cảm giác vui vẻ và sự phấn khích, là một món ăn giúp làm dịu dàng từng cơn nóng bức và tạo ra niềm vui trong cuộc sống hàng ngày. 🍦🍨🍫🍓🌞",
    user_id: 6,
    comments: [
      "Kem lúc nào cũng ngon nhất, đặc biệt vào mùa hè 🍦🌞.",
      "Chắc chắn là mọi người sẽ thích hình ảnh kem mềm béo và ngon lành này 🍨😋.",
      "Một cốc kem tươi mát là cách tốt nhất để làm dịu cơn nóng mùa hè 🌡️🍧.",
      "Kem có vị ngọt ngào làm tôi nhớ về những kỷ niệm hạnh phúc 🍦❤️.",
      "Khi thấy hình ảnh kem, tôi tức thì muốn đến quầy kem gần nhất 🏃🍦.",
      "Kem là một món tráng miệng hoàn hảo sau bất kỳ bữa ăn nào 🍨🍽️.",
      "Sự kết hợp của màu sắc và vị trái cây trong kem thật tuyệt vời 🌈🍧.",
      "Mùi thơm của kem tạo ra một không gian hạnh phúc trong không gian xung quanh 🍦🌸.",
      "Hình ảnh kem luôn làm tôi nở nụ cười và tạo cảm giác thoải mái 🍨😄.",
      "Kem là niềm vui đơn giản nhưng không bao giờ làm chán ngán 🍦🎉.",
    ],
  },
  {
    image_name: "Cỏ may mắn",
    image_url: "./img/co-may-man.jpg",
    image_desc:
      "Hình ảnh cỏ may mắn thường là một bãi cỏ xanh mướt với những bông cỏ dại, trong đó có những chiếc lá ba lá may mắn đặc trưng. Những chiếc lá này có bốn lá thay vì ba như loài cỏ thông thường, và chúng thường được coi là biểu tượng của sự may mắn và tình duyên.\n\nHình ảnh cỏ may mắn thường đem lại cảm giác của sự may mắn và điều tốt lành trong cuộc sống. Người ta thường tìm kiếm và giữ những chiếc lá ba lá may mắn để mang theo hoặc đặt trong ngôi nhà như một biểu tượng để thu hút sự may mắn. Loài cỏ này cũng thường được dùng trong ngày lễ Lá Thứ 17 vào ngày 17 tháng 3 để kỷ niệm và tưởng nhớ nguồn gốc của ngày St. Patrick và văn hóa Ireland.\n\nHình ảnh cỏ may mắn thường gợi lên cảm giác về tình yêu, hy vọng và sự kỳ diệu, và nó là một biểu tượng quen thuộc trong nhiều nền văn hóa trên khắp thế giới. 🍀🍃🌈✨🤞",
    user_id: 6,
    comments: [
      "Cỏ may mắn luôn là biểu tượng của sự may mắn và niềm hy vọng 🍀🌈.",
      "Khi tôi thấy cỏ may mắn, tôi luôn tin rằng mọi điều tốt lành sẽ đến với mình 🌟😊.",
      "Hình ảnh những chiếc lá bốn lá tạo ra một cảm giác kỳ diệu và tạo sự tươi vui trong tâm hồn 🌱😃.",
      "Cỏ may mắn là một biểu tượng của sự thịnh vượng và thành công 🍀💰.",
      "Tìm thấy một chiếc lá bốn lá là như tìm thấy kho báu của cuộc đời bạn 🍀💎.",
      "Hình ảnh cỏ may mắn là một lời nhắc nhở rằng cần phải lạc quan và luôn tìm kiếm điều tốt lành xung quanh 🌞🍀.",
      "Nhìn vào cỏ may mắn, tôi thấy mình được bao quanh bởi sự may mắn và tình yêu của cuộc sống 🥰🍀.",
      "Cỏ may mắn là biểu tượng của tình bạn và lòng tốt, nó luôn mang lại niềm vui cho người khác 🍀❤️.",
      "Tôi luôn tin rằng cỏ may mắn mang lại may mắn thực sự, không chỉ trong cuộc sống mà còn trong tâm hồn 🌠🍀.",
      "Hình ảnh cỏ may mắn là sự kết hợp hoàn hảo giữa tự nhiên và niềm tin vào điều kỳ diệu 🌿🌟.",
    ],
  },
  {
    image_name: "Tháp Eiffel",
    image_url: "./img/Thap-Eiffel.jpg",
    image_desc:
      "Hình ảnh tháp Eiffel là một biểu tượng nổi tiếng của Paris và của Pháp. Tháp Eiffel là một công trình kiến trúc vô cùng độc đáo với cấu trúc sắt thép vững chắc, vững vàng trên bờ sông Seine. Tháp Eiffel nổi bật với ba tầng và đỉnh tháp cao vút, tạo nên một dáng vẻ đặc biệt và độc đáo.\n\nHình ảnh tháp Eiffel thường là một biểu tượng của lãng mạn và vẻ đẹp của Paris, với ánh đèn lung linh khi màn đêm buông xuống. Tháp Eiffel thường được ghép cùng với cặp đôi dạo chơi, hoặc là nơi tận hưởng một ly rượu vang ngon trong một quán café ven sông.\n\nTháp Eiffel là một tác phẩm nghệ thuật kiến trúc độc đáo và có sức hút to lớn, thu hút hàng triệu du khách từ khắp nơi trên thế giới đến tham quan và chiêm ngưỡng vẻ đẹp của nó cũng như tận hưởng tầm nhìn tuyệt vời từ đỉnh tháp. Hình ảnh tháp Eiffel thường kích thích cảm giác tò mò và kỳ vọng, và là biểu tượng của tình yêu và lãng mạn. 🗼🌆🇫🇷❤️🌃",
    user_id: 7,
    comments: [
      "Tháp Eiffel ở Paris là biểu tượng tuyệt đẹp của nền văn hóa Pháp 🗼🇫🇷.",
      "Khám phá Tháp Eiffel vào ban đêm là trải nghiệm kỳ diệu, khi nó sáng rực như một ngôi sao lấp lánh trên bầu trời 🌃✨.",
      "Hình ảnh của Tháp Eiffel là một minh chứng cho kiến trúc đẹp và tinh tế của con người 🏛️👌.",
      "Cảm giác khi đứng dưới chân Tháp Eiffel và ngắm lên trên là một trải nghiệm tuyệt vời, như bạn đang tiến đến bầu trời 🏞️🌌.",
      "Tháp Eiffel nổi bật giữa Paris, tạo nên một bức tranh quyến rũ cho thành phố tình yêu 🌆❤️.",
      "Hình ảnh Tháp Eiffel thường gợi lên ý tưởng về tình yêu lãng mạn và cuộc sống đẳng cấp 🥂💑.",
      "Ngắm nhìn Tháp Eiffel từ sông Seine là một trải nghiệm đặc biệt, khi bạn thấy nó phản chiếu trên nước 🚤🌉.",
      "Tháp Eiffel là biểu tượng của Paris, nơi mọi người đến để thực hiện những ước mơ và kỷ niệm đáng nhớ 🗼🎉.",
      "Tháp Eiffel có một vẻ đẹp vượt thời gian, và không có gì so sánh được với nó trong kiến trúc thế giới 🏰🌟.",
      "Hình ảnh Tháp Eiffel là một lời nhắc nhở về vẻ đẹp và quyền lực của kiến trúc con người 🏗️🌇.",
    ],
  },
  {
    image_name: "Rồng",
    image_url: "./img/dragon.jpg",
    image_desc:
      "Hình ảnh rồng thường là một sinh vật huyền bí và hùng mạnh trong nhiều truyền thuyết và văn hóa trên khắp thế giới. 🐉 Rồng thường được miêu tả là một sinh vật có thân hình khổng lồ, có thể bay và thường có vảy ánh kim hoặc lớp vảy chắc chắn. Chúng thường có đầu có răng nanh và mắt lớn, cùng với một cái rốn và sự cản trở.\n\nHình ảnh rồng thường đại diện cho sức mạnh, quyền lực và sự kiểm soát. Trong một số văn hóa, rồng có thể thể hiện sự tốt lành và bảo vệ, trong khi ở nơi khác, chúng có thể được coi là biểu tượng của sự khốn nạn và nguy hiểm. Rồng thường có một vai trò quan trọng trong truyền thuyết và huyền thoại, thường được liên kết với sự sáng tạo, bí ẩn và truyền thống văn hóa. 🐲\n\nHình ảnh rồng thường kích thích sự tò mò và mê hoặc, và chúng luôn là một phần quan trọng trong nghệ thuật và văn hóa truyền thống, từ tranh vẽ đến kiến trúc và trang trí. 🎨🏛️🐲",
    user_id: 7,
    comments: [
      "Hình ảnh rồng luôn đầy ấn tượng và kỳ bí, đánh thức sự tò mò của tôi 🐉🔥.",
      "Vẻ đẹp của hình ảnh rồng thường kết hợp sự mạnh mẽ và uyển chuyển, tạo nên một hiện thân vĩ đại 🐲💪.",
      "Rồng thường được coi là biểu tượng của quyền lực và sức mạnh, và hình ảnh của họ thường rất thú vị 🌋🦚.",
      "Hình ảnh rồng thường xuất hiện trong văn hóa và truyền thuyết của nhiều dân tộc trên khắp thế giới 🌎📜.",
      "Rồng có sức mạnh hủy diệt nhưng cũng có khả năng bảo vệ và đem lại may mắn cho người khác 🍀🔥.",
      "Cùng với sự hoành tráng và sức mạnh, hình ảnh rồng thường mang theo một vẻ đẹp hoang dã và bí ẩn 🌿🌟.",
      "Rồng là biểu tượng của sự sáng tạo và tưởng tượng, luôn là nguồn cảm hứng cho nghệ thuật và văn hóa 🎨📚.",
      "Hình ảnh rồng có thể là sự kết hợp của các yếu tố khác nhau như rắn, cá, và thú hoang dã, tạo nên một sinh vật độc đáo 🐍🐟.",
      "Rồng thường xuất hiện trong trang trí và nghệ thuật, tạo nên một cảm giác mê hoặc và quyến rũ 🎉🌌.",
      "Hình ảnh rồng thường đưa ta vào một thế giới huyền bí và thần thoại, nơi mọi điều có thể xảy ra 🌠🧙‍♂️.",
    ],
  },
  {
    image_name: "Mùa xuân",
    image_url: "./img/spring.jpg",
    image_desc:
      "Hình ảnh mùa xuân thường là một cảnh sắc tự nhiên rực rỡ, với cây cỏ bắt đầu nảy mầm, hoa cúc, hoa anh đào và hoa dại bắt đầu nở rộ. Cây lá mới xanh mướt đang nở hoa và động vật đang trở lại sau mùa đông dài. Các con sông đang tràn đầy nước và mặt trời ấm áp len lỏi qua những tán cây.\n\nHình ảnh mùa xuân thường mang theo một cảm giác của sự tái sinh và hy vọng. Nó đánh dấu sự trở lại của sự sống sau một mùa đông giá lạnh, và người ta thường kỷ niệm nó với lễ hội và hoạt động ngoài trời. Mùa xuân cũng là thời gian cho sự tươi mới và màu sắc với hoa kết hợp với mùi hương thơm ngát, làm cho không gian trở nên rạng ngời.\n\nHình ảnh mùa xuân thường gợi lên cảm giác của niềm vui và nơi ấm áp, với ngày dài và ánh nắng rạng ngời, là thời gian lý tưởng để thư giãn ngoài trời và tham quan thiên nhiên. 🌸🌱🌼🌞🦋",
    user_id: 8,
    comments: [
      "Mùa xuân là thời gian của sự tái sinh, khi thiên nhiên bắt đầu trổ hoa và rực rỡ 🌸🌱.",
      "Hình ảnh những bông hoa màu sắc nở rộ trên đồng cỏ làm tôi thấy lòng mình tràn đầy niềm vui 🌼😃.",
      "Không gian với màu xanh tươi mát và ánh nắng ấm áp làm cho mùa xuân trở nên đáng yêu và ấm cúng ☀️🍃.",
      "Mùa xuân là thời gian của hạnh phúc và hy vọng, khi tất cả thứ sống lại sau mùa đông lạnh lẽo 🌞🌷.",
      "Hình ảnh bầy chim hót ca trên cây làm cho mùa xuân trở nên thú vị và đẹp đẽ 🐦🎶.",
      "Ngắm nhìn cánh hoa anh đào rụng xuống đất là như thấy thiên đàng rải hoa 🌸❄️.",
      "Mùa xuân là thời gian tuyệt vời để đi dạo và thưởng thức sự nở hoa của cây cối 🚶‍♂️🌺.",
      "Hình ảnh của những đám mây trắng trên bầu trời xanh trong mùa xuân làm tôi cảm thấy bình yên 🌤️⛅.",
      "Mùa xuân là thời điểm tuyệt vời để trồng cây và tạo nên một vườn hoa tươi đẹp 🌳🌻.",
      "Hình ảnh ánh nắng và nhiệt độ dịu dàng làm tôi cảm thấy cuộc sống tràn đầy lạc quan và hy vọng 🌞🌼.",
    ],
  },
  {
    image_name: "Cún hiệp sĩ",
    image_url: "./img/Tuat.jpg",
    image_desc:
      "Hình ảnh cún hiệp sĩ thường là một hình ảnh đáng yêu và lý thú về một chú chó với trang phục hiệp sĩ, bao gồm mũ sắt, áo giáp, và kiếm hoặc lưỡi liềm. Cún hiệp sĩ thường được miêu tả như một người bạn đồng hành đáng tin cậy, sẵn sàng bảo vệ và chiến đấu cho công lý.\n\nHình ảnh này thường mang theo một ý nghĩa của sự dũng cảm, trung thành và sự tận tụy. Cún hiệp sĩ thường xuất hiện trong câu chuyện và tranh minh họa cho trẻ em, là biểu tượng của sự bảo vệ và lòng can đảm. Chúng là nguồn cảm hứng cho trẻ em về việc đối mặt với khó khăn và lập nên những giá trị đạo đức.\n\nHình ảnh cún hiệp sĩ thường kích thích sự tưởng tượng và niềm vui, và chúng thường là một phần không thể thiếu trong văn hóa dân gian và truyện cổ tích. 🐶⚔️🛡️👑🏰",
    user_id: 8,
    comments: [
      "Cún hiệp sĩ là biểu tượng của sự dũng cảm và trung thành 🐶⚔️.",
      "Các chú cún này trong bộ áo giáp sẽ chắc chắn bảo vệ vương quốc của chúng ta 🏰🐾.",
      "Nhìn vào hình ảnh cún hiệp sĩ, tôi cảm thấy có niềm tin vào sự bảo vệ và an toàn 🛡️❤️.",
      "Cún hiệp sĩ luôn sẵn sàng chống lại mọi thử thách và bảo vệ những người yêu thương 🦸‍♂️🐕.",
      "Hình ảnh cún hiệp sĩ là một minh chứng cho tình bạn và đoàn kết trong mọi cuộc phiêu lưu 🤝🏞️.",
      "Mỗi cún hiệp sĩ là một anh hùng riêng biệt, và họ đã học được nghệ thuật của sự kiên nhẫn và khéo léo 🌟🛡️.",
      "Nhìn thấy họ, tôi thấy mình có sức mạnh và can đảm để đối mặt với mọi khó khăn 🦁💪.",
      "Hình ảnh cún hiệp sĩ nhắc nhở tôi rằng trong mỗi con người, có một tinh thần anh hùng đang chờ đợi để thể hiện 🌠🐶.",
      "Các chú cún này không chỉ là thú cưng, mà còn là bạn đồng hành trung thành, luôn ở bên cạnh khi cần 🐕🤗.",
      "Hình ảnh cún hiệp sĩ là một biểu tượng của niềm tin, lòng can đảm và sự tận thế 🐕⚔️.",
    ],
  },
  {
    image_name: "Phố cổ Hội An",
    image_url: "./img/pho-co-Hoi-An.jpg",
    image_desc:
      "Hình ảnh phố cổ Hội An là một cảnh quan thần tiên, nằm ở thành phố cổ cùng tên ở Việt Nam. Phố cổ Hội An thường được miêu tả bởi các ngôi nhà cổ được xây dựng trong kiến trúc truyền thống, với màu sắc và chi tiết tinh tế. Những ngôi nhà này thường nằm ven sông, tạo nên một phong cảnh đáng yêu với cây cầu gỗ cổ kính nối liền.\n\nHình ảnh phố cổ Hội An thường thể hiện sự hài hòa và bình yên, với các con đường nhỏ đáng yêu được đèn lồng lung linh vào ban đêm. Những quán cà phê, cửa hàng thủ công và các quán ẩm thực truyền thống tạo nên một không gian vô cùng phô trương và độc đáo.\n\nPhố cổ Hội An cũng nổi tiếng với các lễ hội ánh sáng lung linh, nơi hàng trăm ngọn đèn lồng sáng lên trên các con đường cổ kính. Hình ảnh này thường gợi lên cảm giác của lễ hội và văn hóa độc đáo của Việt Nam, đồng thời tạo nên một không gian ấm áp và đáng nhớ cho du khách. 🏮🏘️🌙🍜🌸",
    user_id: 9,
    comments: [
      "Phố cổ Hội An là một biểu tượng của vẻ đẹp cổ điển và sự lưu giữ của lịch sử 🏮🏯.",
      "Hình ảnh đèn lồng lung linh khiến phố cổ Hội An trở thành một điểm đến tuyệt vời vào ban đêm 🏮✨.",
      "Cảm giác đi dạo trong lối vào hẹp và xinh đẹp của Hội An là không thể quên 🏘️❤️.",
      "Phố cổ Hội An với các cửa hàng truyền thống và món ăn ngon là thiên đàng của những người yêu ẩm thực 🍜🍢.",
      "Những góc phố cổ đẹp mắt tạo nên nền văn hóa và kiến trúc độc đáo của Hội An 🏰🌸.",
      "Hình ảnh cầu Nhật Bản xưa tại Hội An là một bảng tranh sống động về sự hòa hợp giữa hai nền văn hóa 🌉🎎.",
      "Phố cổ Hội An mang lại cảm giác thư giãn và thanh bình, là nơi tuyệt vời để trốn thoát khỏi cuộc sống hối hả 🌆🍃.",
      "Sự hòa quyện giữa lịch sử và hiện đại làm cho Hội An trở thành một điểm đến độc đáo và thú vị 🏡🏞️.",
      "Khi bạn đi dạo trong phố cổ Hội An, bạn cảm nhận sự ấm áp và niềm hạnh phúc của người dân địa phương 🌞👪.",
      "Hình ảnh phố cổ Hội An là một lời gợi nhớ về vẻ đẹp của Việt Nam và những giá trị văn hóa độc đáo của nước này 🇻🇳🌅.",
    ],
  },
  {
    image_name: "Chú chim nhỏ",
    image_url: "./img/chim.jpg",
    image_desc:
      "Hình ảnh chú chim nhỏ thường là một hình ảnh dễ thương và đáng yêu về một con chim có kích thước nhỏ, với lông màu sáng và đôi mắt tinh nghịch. Chú chim nhỏ thường có màu lông bắt mắt và mỏi sáng, và chúng có thể được thấy đang bay lượn, ngồi trên cành cây hoặc đang tìm kiếm thức ăn.\n\nHình ảnh này thường mang theo cảm giác của sự tự do và thoải mái, với chú chim nhỏ tỏ ra vô cùng linh hoạt trong các hoạt động hàng ngày của nó. Chúng thường là biểu tượng của sự gần gũi với tự nhiên và là một phần của cảnh quan xung quanh.\n\nHình ảnh chú chim nhỏ thường làm cho người ta cảm thấy vui vẻ và thoải mái, với vẻ đáng yêu và ngây thơ của chúng. Chúng thường là một phần quan trọng của thế giới tự nhiên và luôn là một nguồn cảm hứng cho người yêu thiên nhiên và động vật. 🐦🌿🌼😊🌳",
    user_id: 9,
    comments: [
      "Chú chim nhỏ này trông thật đáng yêu và đáng yêu quá! 🐦❤️",
      "Sự dễ thương của chú chim nhỏ này có thể làm tan chảy trái tim bất kỳ ai 🥰🐤.",
      "Nhìn thấy hình ảnh chú chim nhỏ, tôi thấy mình như được kết nối với tự nhiên và thế giới xung quanh 🌿🐦.",
      "Chú chim nhỏ đang bay vút lên trời, tôi cảm thấy mình cũng đang bay lượn tự do cùng anh ấy 🌈✈️.",
      "Màu lông rực rỡ và sự nhanh nhẹn của chú chim nhỏ khiến tôi cười và thấy vui vẻ 🌼😄.",
      "Tôi luôn thấy hình ảnh của những chú chim nhỏ là một minh chứng cho sự đa dạng và quyền tồn tại của mọi loài 🌍🐥.",
      "Chú chim nhỏ có khả năng làm những điều tuyệt vời, dù trong việc tìm kiếm thức ăn hoặc xây tổ cho tổ ấm của mình 🏡🍀.",
      "Hình ảnh chú chim nhỏ thường khiến tôi nhớ đến những giây phút yên bình và thú vị trong thiên nhiên 🌳📸.",
      "Chú chim nhỏ là một biểu tượng của sự hiếu kỳ và tò mò, một lời nhắc nhở để không bao giờ ngừng học hỏi 📚🐣.",
      "Nhìn thấy hình ảnh chú chim nhỏ, tôi luôn cảm thấy lòng mình tràn đầy niềm tin và hy vọng 🌟🦜.",
    ],
  },
  {
    image_name: "Bãi biển",
    image_url: "./img/beach.png",
    image_desc:
      "Hình ảnh bãi biển thường là một phong cảnh tuyệt đẹp với cát trắng mịn, biển xanh biếc và bầu trời xanh rộn ràng. Bãi biển là nơi tuyệt vời để thư giãn và tận hưởng nắng, biển và cát. Cảm giác của cát mềm mịn dưới chân và sóng biển vỗ nhẹ làm cho người ta cảm thấy thoải mái và bình yên.\n\nHình ảnh bãi biển thường kích thích sự tĩnh lặng và thư giãn, với tiếng sóng biển dịu dàng làm lắng nghe và mát lành của biển trong lành. Bãi biển cũng là nơi tuyệt vời để tham gia vào các hoạt động ngoại trời như bơi biển, tắm nắng, lướt ván biển và nhiều hoạt động thú vị khác.\n\nHình ảnh bãi biển thường mang theo cảm giác của sự tự do và khám phá, và nó luôn là một địa điểm yêu thích cho những người tìm kiếm giây phút thoải mái và vui vẻ giữa thiên nhiên tươi đẹp. 🏖️🌊🌞🐚👙",
    user_id: 10,
    comments: [
      "Bãi biển là nơi tuyệt vời để thư giãn và nghỉ ngơi, đặc biệt sau một tuần làm việc căng thẳng 🏖️🌞.",
      "Âm thanh sóng biển và cát nắng tạo nên bản hòa nhạc tự nhiên giúp tôi thấy thật thư giãn 🌊🎶.",
      "Hình ảnh bãi biển với ánh nắng mặt trời làm cho tôi nghĩ đến kỳ nghỉ hoàn hảo 🌴🌅.",
      "Dạo chơi trên bãi cát trắng mịn là cách tốt để giảm căng thẳng và thư giãn tâm hồn 🏝️😌.",
      "Bãi biển luôn đánh thức lòng mê mải và kích thích sự khám phá 🐚🔍.",
      "Màu sắc nước biển và bầu trời không giới hạn làm cho tôi cảm thấy như mình ở trong một thiên đàng 🌊🌈.",
      "Hình ảnh bãi biển với người chơi lướt sóng là biểu tượng của sự mạo hiểm và thú vị 🏄🤙.",
      "Ngắm hoàng hôn trên biển là một trải nghiệm thần kỳ, đầy sự romantisme và tĩnh lặng 🌅❤️.",
      "Bãi biển là nơi tôi có thể kết nối mạnh mẽ với tự nhiên và cảm nhận sức mạnh của biển cả 🌊💪.",
      "Mỗi lần đặt chân lên bãi biển, tôi cảm thấy minh chứng cho vẻ đẹp và sức mạnh của thiên nhiên 🌏🌞.",
    ],
  },
  {
    image_name: "Sách",
    image_url: "./img/book.jpg",
    image_desc:
      "Hình ảnh sách thường là một cảnh sắc đầy tri thức và mở cửa vào thế giới của kiến thức và trí tưởng tượng. Sách có thể là một kệ sách đầy màu sắc, chứa những tác phẩm văn học, hướng dẫn, tiểu sử hoặc triết học. Hình ảnh này thường mang theo sự tĩnh lặng và tư duy, với cuốn sách mở ra như cửa vào một thế giới mới.\n\nSách thường là biểu tượng của học hỏi và khám phá. Chúng có thể là cầu nối tới những câu chuyện, ý tưởng và tri thức, là nguồn cảm hứng và kiến thức cho người đọc. Hình ảnh sách thường kích thích sự tò mò và khám phá, và thúc đẩy tình yêu đối với việc đọc và học hỏi.\n\nSách có sức mạnh thay đổi cuộc sống của con người và tạo nên sự thay đổi trong tư duy của họ. Hình ảnh sách thường là biểu tượng của sự biến đổi và truyền cảm hứng cho việc học hỏi và phát triển. 📚📖🌍🧠📝",
    user_id: 10,
    comments: [
      "Hình ảnh những cuốn sách là một cửa sổ mở ra thế giới vô tận của tri thức 📚🌍.",
      "Cuốn sách nào cũng là một phiêu lưu mới đang chờ đợi bạn khám phá 🚀📖.",
      "Khám phá thế giới thông qua sách là một trải nghiệm thú vị và bổ ích 🌏📕.",
      "Mùi của sách cũ luôn đem lại cảm giác thú vị của thời gian đã trôi qua 📜👃.",
      "Sách không chỉ là nguồn kiến thức mà còn là một cách tuyệt vời để thư giãn và thả mình vào câu chuyện 📖😌.",
      "Hình ảnh các đầu sách đứng sẵn sàng trên kệ là lời mời đầy hứa hẹn để bắt đầu một cuộc phiêu lưu mới 📚🎉.",
      "Tôi yêu cách mà sách có thể thay đổi cuộc sống của bạn và mang đến cái nhìn mới về thế giới 🌟📔.",
      "Hình ảnh người đọc sách có thể thấy mình mắc kẹt trong câu chuyện, như một phần của thế giới tưởng tượng 🤩📚.",
      "Sách là bạn đồng hành tốt nhất của bạn, luôn sẵn sàng để bạn mở ra và tiếp tục cuộc hành trình tri thức 📚🚶.",
      "Sách là món quà của tình yêu và tri thức, một kho báu không giới hạn 📚❤️.",
    ],
  },
];

// TẠO HÌNH ẢNH

async function createImage(count) {
  let arr = images[count].image_url.split("/");
  let name = arr[arr.length - 1];

  let res = await fetch(images[count].image_url);
  let myBlob = await res.blob();
  const myFile = new File([myBlob], name, { type: myBlob.type });

  let { image_name, image_desc } = images[count];
  let user_id = count % 2 == 0 ? (count + 2) / 2 : (count + 1) / 2;

  res = await uploadImageService(myFile, image_name, image_desc, user_id);
  console.log(res);

  if (++count < images.length) {
    createImage(count);
  }
}

qS("#create-image").onclick = () => {
  images = shuffle(images);
  createImage(0);
};

// LƯU HÌNH ẢNH

let saveCount = 0;
let userLength;

async function saveRandomImages(userCount, imgCount) {
  await saveImageService(users[userCount], imgCount + 1);
  console.log(++saveCount);

  if (++userCount < userLength) {
    saveRandomImages(userCount, imgCount);
  } else if (++imgCount < images.length) {
    users = shuffle(users);
    userLength = getRndInteger(5, 10);
    saveRandomImages(0, imgCount);
  }
}

qS("#save").onclick = () => {
  users = shuffle(users);
  userLength = getRndInteger(5, 10);
  saveRandomImages(0, 0);
};

// BÌNH LUẬN

let commentCount = 0;

async function getComments(imgCount) {
  let res = await getImageDetailService(imgCount + 1);
  let { image_name } = res.data.content;

  let index = images.findIndex((image) => image.image_name == image_name);
  let comments = images[index].comments;

  users = shuffle(users);
  return shuffle(comments);
}

async function commentImages(imgCount, comments, uCount) {
  let user_id =
    uCount < users.length ? users[uCount] : getRndInteger(1, users.length);

  await saveCommentService(comments[uCount], user_id, imgCount + 1);
  console.log(++commentCount);

  if (++uCount < comments.length) {
    commentImages(imgCount, comments, uCount);
  } else if (++imgCount < images.length) {
    let comments = await getComments(imgCount);

    commentImages(imgCount, comments, 0);
  }
}

qS("#comment").onclick = async () => {
  let comments = await getComments(0);

  commentImages(0, comments, 0);
};

// THÍCH

const types = ["good idea", "love", "thanks", "wow", "haha"];
let reactionCount = 0;

async function createRandomReactions(uCount, imgCount) {
  let type = types[getRndInteger(0, types.length - 1)];
  await createImageReactionService(type, users[uCount], imgCount + 1);
  console.log(++reactionCount);
  if (++uCount < userLength) {
    createRandomReactions(uCount, imgCount);
  } else if (++imgCount < images.length) {
    users = shuffle(users);
    userLength = getRndInteger(5, 10);
    createRandomReactions(0, imgCount);
  }
}

qS("#reaction").onclick = () => {
  users = shuffle(users);
  userLength = getRndInteger(5, 10);
  createRandomReactions(0, 0);
};

// THEO DÕI

async function followRandomUser(uCount, followCount) {
  let shouldFollow = getRndInteger(0, 1);
  let followingId = followCount + 1;

  if (users[uCount] != followingId) {
    if (shouldFollow == 1) {
      await followUserService(users[uCount], followingId);
      console.log(`qS{users[uCount]} theo dõi qS{followingId}`);
      if (++followCount < users.length) {
        followRandomUser(uCount, followCount);
      } else if (++uCount < users.length) {
        followRandomUser(uCount, 0);
      }
    } else {
      if (++followCount < users.length) {
        followRandomUser(uCount, followCount);
      } else if (++uCount < users.length) {
        followRandomUser(uCount, 0);
      }
    }
  } else {
    if (++followCount < users.length) {
      followRandomUser(uCount, followCount);
    } else if (++uCount < users.length) {
      followRandomUser(uCount, 0);
    }
  }
}

qS("#follow").onclick = () => {
  users = shuffle(users);
  followRandomUser(0, 0);
};

// ẢNH ĐẠI DIỆN

async function updateAvatar(uCount) {
  let res = await getUserByIdService(users[uCount]);
  let { email } = res.data.content;

  let index = peopleInVietnam.findIndex((user) => user.email == email);

  let arr = peopleInVietnam[index].avatar.split("/");
  let name = arr[arr.length - 1];

  res = await fetch(peopleInVietnam[index].avatar);
  let myBlob = await res.blob();
  const myFile = new File([myBlob], name, { type: myBlob.type });

  res = await updateUserAvatarService(users[uCount], myFile);
  console.log(res);

  if (++uCount < users.length) {
    updateAvatar(uCount);
  }
}

qS("#avatar").onclick = () => {
  updateAvatar(0);
};

// TEST

qS("#random-num-btn").onclick = () => {
  let min = qS("#min").value * 1;
  let max = qS("#max").value * 1;
  console.log(min, max);

  let inputs = qSA(".data .random-input");
  if (inputs.length > 0) {
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = getRndInteger(min, max);
    }
  }
};

qS("#random-arr-btn").onclick = () => {
  let min = qS("#min").value * 1;
  let max = qS("#max").value * 1;

  let arr = Array.from(Array(max - min + 1), (_, index) => min + index);
  arr = shuffle(arr);

  let inputs = qSA(".data .random-input");
  if (inputs.length > 0) {
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "[" + arr + "]";
    }
  }
};

qS("#random-half-arr-btn").onclick = () => {
  let min = qS("#min").value * 1;
  let max = qS("#max").value * 1;

  let length = getRndInteger(Math.floor((max + min) / 2), max);
  let arr = Array.from(Array(max - min + 1), (_, index) => min + index);
  arr = shuffle(arr).slice(0, length);

  let inputs = qSA(".data .random-input");
  if (inputs.length > 0) {
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = "[" + arr + "]";
    }
  }
};

qS("#save-comment").onclick = async () => {
  try {
    let image_id = qS(".comment .image-id").value * 1;
    let usersCm = JSON.parse(qS(".comment .user-id").value);
    let comments = JSON.parse(qS("#comment-content").value);

    if (image_id == "" || usersCm.length == 0 || comments.length == 0) return;
    commentAnImage(image_id, usersCm, comments, 0);
  } catch (err) {
    console.log(err);
  }
};

async function commentAnImage(image_id, usersCm, comments, cmCount) {
  let user_id;
  if (cmCount < usersCm.length) {
    user_id = usersCm[cmCount];
  } else {
    user_id = usersCm[getRndInteger(0, usersCm.length - 1)];
  }
  let res = await saveCommentService(comments[cmCount], user_id, image_id);
  console.log(res);

  if (++cmCount < comments.length) {
    commentAnImage(image_id, usersCm, comments, cmCount);
  }
}

qS("#save-image").onclick = async () => {
  try {
    let image_id = qS(".save .image-id").value * 1;
    let usersSave = JSON.parse(qS(".save .user-id").value);

    if (image_id == "" || usersSave.length == 0) return;
    saveAnImage(image_id, usersSave, 0);
  } catch (err) {
    console.log(err);
  }
};

async function saveAnImage(image_id, usersSave, uCount) {
  let res = await saveImageService(usersSave[uCount], image_id);
  console.log(res);

  if (++uCount < usersSave.length) {
    saveAnImage(image_id, usersSave, uCount);
  }
}

qS("#react-image").onclick = () => {
  try {
    let image_id = qS(".reaction .image-id").value * 1;
    let usersReaction = JSON.parse(qS(".reaction .user-id").value);

    if (image_id == "" || usersReaction.length == 0) return;
    createReaction(image_id, usersReaction, 0);
  } catch (err) {
    console.log(err);
  }
};

async function createReaction(image_id, usersReaction, uCount) {
  let type = types[getRndInteger(0, types.length - 1)];
  qS("#reaction-type").value = type;

  let res = await createImageReactionService(
    type,
    usersReaction[uCount],
    image_id
  );
  console.log(res);
  if (++uCount < usersReaction.length) {
    createReaction(image_id, usersReaction, uCount);
  }
}

qS("#follow-user").onclick = () => {
  try {
    let following_id = qS(".follow .following-id").value * 1;
    let followers = JSON.parse(qS(".follow .follower-id").value);

    if (following_id == "" || followers.length == 0) return;
    followUser(following_id, followers, 0);
  } catch (err) {
    console.log(err);
  }
};

async function followUser(following_id, followers, uCount) {
  let res = await followUserService(followers[uCount], following_id);
  console.log(res);

  if (++uCount < followers.length) {
    followUser(following_id, followers, uCount);
  }
}
